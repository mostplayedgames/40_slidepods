﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SPCharacter : MonoBehaviour {

    public GameObject m_objectBallPosition;
	public Transform hazard;
	public Transform speedLines;
	public AudioClip rollSound;
	public AudioClip landSound;
	public AudioClip jumpSound;

	public bool rolling = false;
	public float rollDecay = 0.1f;
	public float rollDecayTime = 0.25f;

	public bool airBall = false;

	private Rigidbody rb;

	public Vector3 speed;

	private void Awake()
	{
		rb = GetComponent<Rigidbody>();
		
	}

	// Use this for initialization
	void Start () {
		rollDecay = rollDecayTime;
		rb.velocity = new Vector3(speed.x, speed.y, 200f);
		//	rb.velocity = new Vector3(speed.x, speed.y, 200f);
	}
	
	// Update is called once per frame
	void Update () {
        speed = rb.velocity;
		
		//this.gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(0, 0, 100f));

		Vector3 camPosition;
        camPosition = ZCameraMgr.instance.transform.position;
        //ZCameraMgr.instance.transform.localPosition = new Vector3(camPosition.x, camPosition.y, this.transform.position.z - 90f);
        if (GameScene.instance.m_eState != GAME_STATE.RESULTS) ZCameraMgr.instance.transform.position = new Vector3(camPosition.x, camPosition.y, this.transform.position.z - 60f);

		Vector3 newHazPos = hazard.position;
		newHazPos.z = transform.position.z;
		hazard.position = newHazPos;

		Vector3 newSpeedLinesPos = speedLines.position;
		newSpeedLinesPos.z = transform.position.z + 300;
		speedLines.position = newSpeedLinesPos;
		

		if (!rolling && collisionCount <= 0)
		{
			rb.velocity = new Vector3(speed.x, speed.y, speed.z);
			rollDecay -= Time.deltaTime;

			if (rollDecay <= 0 && !airBall)
			{
				ZAudioMgr.Instance.StopSFX(rollSound);
				ZAudioMgr.Instance.PlaySFX(jumpSound);
				airBall = true;
				rb.velocity = new Vector3(Mathf.Clamp(speed.x, -30, 30), Mathf.Clamp(speed.y, -30, 30), speed.z);
			}
		}
		else
		{
			rb.velocity = new Vector3(Mathf.Clamp(speed.x, -30, 30), Mathf.Clamp(speed.y, -30, 30), 200f);
			rollDecay = rollDecayTime;
			rolling = true;
			airBall = false;
		}

    }
	private int collisionCount = 0;

	void OnCollisionEnter(Collision collision)
    {
		if (collision.gameObject.tag == "Die")
		{
			//            this.gameObject.GetComponent<Rigidbody>().isKinematic = true;
			GameScene.instance.Die();
		}
		else if (collision.gameObject.tag == "Floor") 
		{
			if (!rolling && collisionCount <= 0)
			{
				if (rollDecay <= 0) ZAudioMgr.Instance.PlaySFX(landSound);
				// ZAudioMgr.Instance.PlaySFX(landSound);
				ZAudioMgr.Instance.StopSFX(rollSound);
				ZAudioMgr.Instance.PlaySFXVolume(rollSound, 0.5f, true);
			}
			
			rolling = true;
			rollDecay = rollDecayTime;
			airBall = false;
		}

		collisionCount++;
	}

	private void OnCollisionExit(Collision collision)
	{
	
		collisionCount--;

		if (collisionCount <= 0)
		{
			rolling = false;
		}
	}

	void OnTriggerEnter(Collider collision)
	{
		if (collision.gameObject.tag == "Die")
		{
			//            this.gameObject.GetComponent<Rigidbody>().isKinematic = true;
			GameScene.instance.Die();
		}
	}
}
