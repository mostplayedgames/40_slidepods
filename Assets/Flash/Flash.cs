﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flash : MonoBehaviour 
{
	public static Flash instance;
	public int flashId;

	private void Awake()
	{
		instance = this;
	}

	public void ShowFlash(int newIndex)
	{
		GetComponent<Animator>().Play("Flash");
		flashId = newIndex;
	}

	public void UpdateShop()
	{
		GameScene.instance.UpdateShop(flashId);

	}
}
