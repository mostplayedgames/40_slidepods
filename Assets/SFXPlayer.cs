﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFXPlayer : MonoBehaviour 
{
	public List<AudioClip> sfx;
	public ParticleSystem stars;

	public void PlayShakeSound()
	{
		ZAudioMgr.Instance.PlaySFX(sfx[0]);
	}

	public void PlayYaySound()
	{
		ZAudioMgr.Instance.PlaySFX(GameScene.instance.m_audioSpecialScore);
		stars.Play();
	}

	public void PlayClunkSound()
	{
		ZAudioMgr.Instance.PlaySFX(sfx[1]);
	}

	public void PlayShrinkSound()
	{
		ZAudioMgr.Instance.PlaySFX(sfx[2]);
	}

	public void PlayLiftSound()
	{
		ZAudioMgr.Instance.PlaySFX(sfx[3]);
	}

	public void PlayThudSound()
	{
		ZAudioMgr.Instance.PlaySFX(sfx[4]);
	}
}
