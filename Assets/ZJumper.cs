﻿using UnityEngine;
using System.Collections;

public class ZJumper : MonoBehaviour {

	public float duration;
	public Vector3 positionChange;
	public LeanTweenType easeType;

	public float startDelay = 0;

	public bool isScale = false;

	// Use this for initialization
	void Start () {
		if (startDelay > 0)
			LeanTween.delayedCall (startDelay, Move);
		else
			Move ();


	}

	public void Move()
	{
		LeanTween.moveLocal (this.gameObject, this.transform.localPosition + positionChange, duration).setLoopPingPong().setEase(easeType);

		if (isScale) {
			LeanTween.scaleY (this.gameObject, this.transform.localScale.y - 0.06f, duration).setLoopPingPong().setEase(LeanTweenType.easeInOutExpo);
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
