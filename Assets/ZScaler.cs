﻿using UnityEngine;
using System.Collections;

public class ZScaler : MonoBehaviour {

	public float m_scaleRatio = 0.02f;
	public float m_scaleTime = 0.2f;
	// Use this for initialization
	void Start () {
		LeanTween.scale (this.gameObject, this.gameObject.transform.localScale + new Vector3 (m_scaleRatio, m_scaleRatio, m_scaleRatio), m_scaleTime).setLoopPingPong ().setEase(LeanTweenType.easeInOutQuad);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
