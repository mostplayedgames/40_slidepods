﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour 
{
	public GameObject particles;

	private void OnTriggerEnter(Collider other)
	{
		Collect();
	}

	public void Collect()
	{
		GameScene.instance.AddCoins(1, true);
		GameObject g = ZObjectMgr.Instance.Spawn3D(particles.name, transform.localPosition);
		g.transform.localEulerAngles = new Vector3(90, 180, 180);
		gameObject.SetActive(false);
	}
}
