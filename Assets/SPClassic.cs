﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SPClassic : FDGamesceneInstance
{
	[System.Serializable]
	public class Pattern
	{
		public string id;
		public float minWidth;
		public float maxWidth;
		public float minLength;
		public float maxLength;
	}

    public static SPClassic instance;

	public bool disableSpawn = false;

    public GameObject m_objectRoad;
    public GameObject m_objectCharacter;

	public List<GameObject> m_prefabRoad;
	public List<GameObject> m_prefabUnique;

	public GameObject m_prefabEnemy;
	public GameObject m_prefabArcs;
	public GameObject m_rootSpawnedObjects;

	public Vector3 nextPos;
	public float nextSpawnPoint;
	public Road currBlock;
	public Road nextBlock;
	public int numBlocks;
	public int numBlocksToNextPattern;
	public int numBlocksToNextSpecial;

	public float blockMinWidth = 3;
	public float blockMaxWidth = 10;
	public float maxZ = 4;

	[SerializeField]
	public List<Pattern> patterns;
	public List<Pattern> specialPatterns;
	public string pattern = "SNAKE";
	public int currPattern = 0;

	public int currDir;

	public float nextScore = -168;

	public uint numRingsShown = 5;
	public uint numRingsSpawned = 0;
	public List<MeshRenderer> ringMeshRenderers;
	public List<Material> ringMats;

	public Material roadMat;
	public ParticleSystem particles;
	public Color currColor;

	public Color ringDefaultColor;
	public Color ringScoreColor;

	public Color ringDefaultColorOld;
	public Color ringScoreColorOld;


	public int currRing;

	public float cameraBounds = 2.0f;

	public ParticleSystem playerParticles;

	public bool touched = false;
	public float touchOriginPoint;
	public float scaleFactor;
	public float lastRot;
	public float sensitivity = 50.0f;
	public bool left = false;
	public float colorSensitvity = 1.0f;

	public GameObject coin;
	public GameObject coinParticles;

	public int currentColor = 0;
	public List<Color> roadColors;
	public List<Color> ringColors;
	public List<Color> scoredRingColors;

	public float zFactor = 0;

	void Awake () 
	{
		instance = this;

		for (int x = 0; x < m_prefabUnique.Count; x++)
		{
			if (PlayerPrefs.GetInt("Unique " + x.ToString() + " Encountered", 0) == 1)
			{
				m_prefabUnique[x].GetComponent<Road>().encountered = true;
			}
			else m_prefabUnique[x].GetComponent<Road>().encountered = false;
		}

		ZObjectMgr.Instance.AddNewObject (m_prefabEnemy.name, 3,this.m_rootSpawnedObjects);
		ZObjectMgr.Instance.AddNewObject (m_prefabArcs.name, numRingsShown, this.m_rootSpawnedObjects);

		ZObjectMgr.Instance.AddNewObject(coin.name, 10, this.m_rootSpawnedObjects);
		ZObjectMgr.Instance.AddNewObject(coinParticles.name, 10, this.m_rootSpawnedObjects);


		foreach (GameObject g in m_prefabRoad)
		ZObjectMgr.Instance.AddNewObject(g.name, 3, this.m_rootSpawnedObjects);

		foreach (GameObject g in m_prefabUnique)
			ZObjectMgr.Instance.AddNewObject(g.name, 3, this.m_rootSpawnedObjects);

		currRing = 2;

		scaleFactor = 1080.0f / ((float) Screen.width);
	}

	void Update () {
		if (GameScene.instance.m_eState != GAME_STATE.RESULTS && GameScene.instance.m_eState != GAME_STATE.SHOOT)
		{
			// Camera.main.transform.localPosition = new Vector3(0, 14.5f, -173);
//			m_objectRoad.transform.localEulerAngles = Vector3.zero;

			if (m_objectCharacter.transform.localPosition.y <= nextSpawnPoint)
			{
				// GameScene.instance.Score(1);
				SpawnBlockPlain();
				
			}

			if (m_objectCharacter.transform.localPosition.y <= nextScore + 50)
			{
				nextScore -= 150;
				SpawnNextArc();
			}
		}
		else if (GameScene.instance.m_eState == GAME_STATE.SHOOT)
		{
			if (Input.GetMouseButton(0))
			{
				if (!touched)
				{
					touchOriginPoint = Input.mousePosition.x;
					touched = true;
				}

				// m_objectRoad.transform.localEulerAngles = new Vector3(0, (Input.mousePosition.x - (Screen.width / 2)) / 5f, 0);
				m_objectRoad.transform.localEulerAngles = new Vector3(0, lastRot + ((float)Input.mousePosition.x - touchOriginPoint) * (sensitivity * scaleFactor), 0);

				// Camera.main.transform.localPosition = new Vector3((cameraBounds - (((Input.mousePosition.x * 1.0f) / (Screen.width * 1.0f)) * cameraBounds * 2)) * -0.5f, 14.5f, -173);



				// Debug.Log("Mouse Position : " + Input.mousePosition.x + ", Current Res : " + Screen.currentResolution.width);
			}
			else if (Input.touchCount > 0)
			{

				if (!touched)
				{
					touchOriginPoint = Input.GetTouch(0).position.x;
					touched = true;
				}

				//	m_objectRoad.transform.localEulerAngles = new Vector3(0, (Input.GetTouch(0).position.x - (Screen.width / 2)) / 50f, 0);

				// distance should be calculated relative to screen width, 1080 is normal

				m_objectRoad.transform.localEulerAngles = new Vector3(0, lastRot + (((float) Input.GetTouch(0).position.x - touchOriginPoint) * (sensitivity * scaleFactor)), 0);
			}
			else
			{
				touched = false;
				lastRot = m_objectRoad.transform.localEulerAngles.y;
			}

/*			if (touched)
			{
				if ((m_objectRoad.transform.localEulerAngles.y > 180 && m_objectRoad.transform.localEulerAngles.y < 360) || (m_objectRoad.transform.localEulerAngles.y < 0 && m_objectRoad.transform.localEulerAngles.y > -180))
				{
					left = true;
				}
				else left = false;
			}
			*/

			if (m_objectCharacter.transform.localPosition.y <= nextSpawnPoint && !disableSpawn)
			{
				// GameScene.instance.Score(1);
				if (currBlock) currBlock.Score();
				SpawnBlock();
			}

			if (m_objectCharacter.transform.localPosition.y <= nextScore + 50)
			{
				GameScene.instance.Score();
				nextScore -= 150;
				// SpawnNextArc();
			}

		//	Camera.main.transform.localPosition = new Vector3((m_objectRoad.transform.localEulerAngles.y / 180.0f) * (left ? -cameraBounds : cameraBounds), 14.5f, -173);
		}

		// Tilt Based Color
		//		currColor = Color.HSVToRGB((((m_objectRoad.transform.localEulerAngles.y * colorSensitvity) + 180) % 360)  / 360.0f, 0.75f, 0.75f);


		// Distance Based Color
		/*	
		currColor = Color.HSVToRGB((((Mathf.Abs(m_objectCharacter.transform.localPosition.y) * colorSensitvity) + 180) % 360) / 360.0f, 0.75f, 0.75f);
		roadMat.color = currColor;
		currColor.a = 0.875f;

		particles.startColor = currColor;


		float hue;
		float saturation;
		float value;

		Color.RGBToHSV(ringDefaultColorOld, out hue, out saturation, out value);
		ringDefaultColor = Color.HSVToRGB((Mathf.Abs(m_objectCharacter.transform.localPosition.y) * colorSensitvity + hue % 360), saturation, value);
		*/

		/*
		if (Input.GetKeyDown(KeyCode.S)) pattern = "SNAKE";
		*/
		if (Input.GetKeyDown(KeyCode.Space)) numBlocksToNextSpecial = 0;
		
		// if (Input.GetKeyDown(KeyCode.C)) UpdateColor();
		
	}

	public override void Gamescene_Score()
    {
		playerParticles.Stop();
		playerParticles.Play();

		if (GameScene.instance.m_currentScore % 20 == 0) UpdateColor();
		SpawnNextArc();
    }


	private int currX = 0;
	private Color prevColor;
	private Color prevRingColor;

	public void UpdateColor()
	{
		currentColor++;

		if (currentColor >= ringColors.Count)
		{
			currentColor = 0;
		}

		currX = 0;
		prevColor = roadMat.color;
		prevRingColor = ringDefaultColor;

		for (int x = 1; x <= 20;  x++)
		{
			LeanTween.delayedCall(x * 0.05f, UpdateColorRoutine);
		}
		
		// roadMat.color = roadColors[currentColor];
		// ringDefaultColor = ringColors[currentColor];
	}

	public void UpdateColorRoutine()
	{
		currX++;

		Color colorDiff = roadColors[currentColor] - prevColor;
		colorDiff.a = 1;

		Color colorInc = (currX * 0.05f) * colorDiff;
		colorInc.a = 0;

		roadMat.color = prevColor + colorInc;
		particles.startColor = prevColor + colorInc;

		colorDiff = ringColors[currentColor] - prevRingColor;
		colorDiff.a = 1;

		colorInc = (currX * 0.05f) * colorDiff;
		colorInc.a = 0;

		ringDefaultColor = prevRingColor + colorInc;

		for (int x = 0; x < ringMats.Count; x++)
		{
			if (x != currRing) ringMats[x].color = ringDefaultColor;
			else if (GameScene.instance.m_eState == GAME_STATE.SHOOT) ringMats[currRing].color = scoredRingColors[currentColor]; // ringScoreColor;
			else ringMats[x].color = ringDefaultColor;
		}

		// ringMats[currRing].color = ringScoreColor;

		// roadMat.color = roadColors[currentColor];
		// ringDefaultColor = ringColors[currentColor];
	}

	public void SpawnNextArc()
	{
		ZObjectMgr.Instance.Spawn3D(m_prefabArcs.name, new Vector3(75.2f, 232f - (numRingsSpawned * 150), -18.2f)).transform.localEulerAngles = Vector3.zero;
		numRingsSpawned++;

		for (int x = 0; x < ringMats.Count; x++)
		{
			ringMats[x].color = ringDefaultColor;
		}

		ringMats[currRing].color = scoredRingColors[currentColor]; // ringScoreColor;

		currRing++;
		if (currRing >= ringMats.Count) currRing = 0;
	}

    public override void Gamescene_Die()
    {
		Debug.Log("I died!");
		nextPos = new Vector3(0, 0, 2);
		currBlock = null;
		nextSpawnPoint = 0;
	}


	float m_currentPosition;
	int m_currentIndex;

	public void SpawnInitialLevel()
	{
		m_currentPosition = -500;

		for (int x = 0; x < 50; x++) {
			SpawnLevel ();
		}
	}

	public void SpawnLevel()
	{
		GameObject obj = ZObjectMgr.Instance.Spawn3D (m_prefabEnemy.name, new Vector3 (67.6f, m_currentPosition, -7));
		obj.transform.localScale = new Vector3 (1, 0.003f, 1);
		obj.transform.localEulerAngles = new Vector3 (0, 0, 0);
		obj.transform.localPosition += new Vector3 (Random.Range (-4.1f, 4.1f), 0, 0);

		m_currentPosition -= Random.Range(100f, 300f);
		m_currentIndex++;
	}

	public void SpawnBlock()
	{
		if (!disableSpawn)
		{
			bool firstBlock = false;
			bool lockX = false;
			bool spawnCoin = false;

			if (Random.Range(0, 10) < 7) spawnCoin = true;


			Road newBlock;

			if (!currBlock) // first spawn
			{
				numBlocks = 1;
				newBlock = ZObjectMgr.Instance.Spawn3D(m_prefabRoad[0].name, nextPos).GetComponent<Road>();
				currBlock = newBlock;
				firstBlock = true;

				newBlock.SetDimensions(400, 8);
			}
			else
			{
				numBlocks++; ;
				numBlocksToNextPattern--;
				numBlocksToNextSpecial--;

				if (numBlocksToNextPattern <= 0)
				{
					//				numBlocks = 0;

					patterns.Shuffle();

					pattern = patterns[0].id;

					numBlocksToNextPattern = Random.Range(8, 12);
				}

				if (numBlocksToNextSpecial <= 0)
				{
					numBlocksToNextSpecial = Random.Range(9, 20);
					
					newBlock = ZObjectMgr.Instance.Spawn3D(m_prefabUnique[Random.Range(0, m_prefabUnique.Count)].name, nextPos).GetComponent<Road>();
				}
				else switch (pattern)
				{
					default:
						if (currBlock.unique || currBlock.elevation == ElevationType.Ascend || currBlock.leanLeft || currBlock.leanRight) newBlock = ZObjectMgr.Instance.Spawn3D(m_prefabRoad[0].name, nextPos).GetComponent<Road>();
						else if (currBlock.transform.position.x >= 3) newBlock = ZObjectMgr.Instance.Spawn3D(m_prefabRoad[1].name, nextPos).GetComponent<Road>();
						else if (currBlock.transform.position.x <= -3) newBlock = ZObjectMgr.Instance.Spawn3D(m_prefabRoad[2].name, nextPos).GetComponent<Road>();
						else newBlock = ZObjectMgr.Instance.Spawn3D(m_prefabRoad[Random.Range(1, /*3*/ m_prefabRoad.Count)].name, nextPos).GetComponent<Road>();

						if (numBlocks % 7 == 0) newBlock.SetDimensions((newBlock.usePrevWidth ? currBlock.width : Random.Range(500, 700)), Random.Range(4, 6));
						else newBlock.SetDimensions((newBlock.usePrevWidth ? currBlock.width : Random.Range(250, 500)), Random.Range(6, 10));
					break;
					
					case "BASIC":
						if (currBlock.unique || currBlock.elevation == ElevationType.Ascend || currBlock.leanLeft || currBlock.leanRight) newBlock = ZObjectMgr.Instance.Spawn3D(m_prefabRoad[0].name, nextPos).GetComponent<Road>();
						else newBlock = ZObjectMgr.Instance.Spawn3D(m_prefabRoad[0].name, nextPos).GetComponent<Road>();

						if (numBlocks % 7 == 0) newBlock.SetDimensions((newBlock.usePrevWidth ? currBlock.width : Random.Range(500, 700)), Random.Range(4, 6));
						else newBlock.SetDimensions((newBlock.usePrevWidth ? currBlock.width : Random.Range(250, 500)), Random.Range(6, 10));
					break;
					
					case "RANDOM":
						if (currBlock.unique || currBlock.elevation == ElevationType.Ascend || currBlock.leanLeft || currBlock.leanRight) newBlock = ZObjectMgr.Instance.Spawn3D(m_prefabRoad[0].name, nextPos).GetComponent<Road>();
						else if (currBlock.transform.position.x >= 3) newBlock = ZObjectMgr.Instance.Spawn3D(m_prefabRoad[1].name, nextPos).GetComponent<Road>();
						else if (currBlock.transform.position.x <= -3) newBlock = ZObjectMgr.Instance.Spawn3D(m_prefabRoad[2].name, nextPos).GetComponent<Road>();
//						else newBlock = ZObjectMgr.Instance.Spawn3D(m_prefabRoad[Random.Range(1, /*3*/ m_prefabRoad.Count)].name, nextPos).GetComponent<Road>();
						else newBlock = ZObjectMgr.Instance.Spawn3D(m_prefabRoad[Random.Range(0, /*3*/ m_prefabRoad.Count)].name, nextPos).GetComponent<Road>();

						if (numBlocks % 7 == 0) newBlock.SetDimensions((newBlock.usePrevWidth ? currBlock.width : Random.Range(500, 700)), Random.Range(4, 6));
						else newBlock.SetDimensions((newBlock.usePrevWidth ? currBlock.width : Random.Range(250, 500)), Random.Range(6, 10));
					break;

					case "SNAKE":
						if (currBlock.transform.position.x >= 3) currDir = -1;
						else if (currBlock.transform.position.x <= -3) currDir = 1;

						switch (currDir)
						{
							case 1:
								newBlock = ZObjectMgr.Instance.Spawn3D(m_prefabRoad[2].name, nextPos).GetComponent<Road>();
								break;

							case -1:
								newBlock = ZObjectMgr.Instance.Spawn3D(m_prefabRoad[1].name, nextPos).GetComponent<Road>();
								break;

							default:
								newBlock = ZObjectMgr.Instance.Spawn3D(m_prefabRoad[0].name, nextPos).GetComponent<Road>();
							break;
						}

						newBlock.SetDimensions((newBlock.usePrevWidth ? currBlock.width : Random.Range(350, 450)), Random.Range(7, 9));
					break;
				}
			}
			

			newBlock.transform.localRotation = Quaternion.identity;

			int randomFactor = Random.Range(0, 2);

			if (newBlock.transform.position.x >= 5) randomFactor = 0;
			else if (newBlock.transform.position.x <= -5) randomFactor = 1;

			if (!(currBlock.elevation == ElevationType.Stay && newBlock.elevation == ElevationType.Stay))
			{
				newBlock.transform.localPosition = new Vector3(currBlock.transform.localPosition.x - currBlock.padding.x, nextPos.y, nextPos.z);
				//				newBlock.transform.localPosition = new Vector3(currBlock.transform.localPosition.x, nextPos.y, nextPos.z) - currBlock.padding;
				nextPos.x = newBlock.transform.localPosition.x;
				lockX = true;
			}

			if (newBlock.leanLeft)
			{
				Vector3 newPos = newBlock.transform.localPosition;
				newPos.x = currBlock.transform.localPosition.x;
				newBlock.padding.x -= ((currBlock.width / 2.0f) + (newBlock.width / 2.0f));
				newPos.x += newBlock.padding.x;
				if (newBlock.usePadding) nextPos.x += newBlock.padding.x;
				newBlock.transform.localPosition = newPos;
				lockX = true;
			}
			else if (newBlock.leanRight)
			{
				Vector3 newPos = newBlock.transform.localPosition;
				newPos.x = currBlock.transform.localPosition.x;
				newBlock.padding.x += ((currBlock.width / 2.0f) + (newBlock.width / 2.0f));
				newPos.x += newBlock.padding.x;
				if (newBlock.usePadding) nextPos.x += newBlock.padding.x;
				newBlock.transform.localPosition = newPos;
				lockX = true;
			}
			else
			{
				Vector3 newPos = newBlock.transform.localPosition;
				if (!firstBlock && !newBlock.forceCenterCurrent) newPos.x += Random.Range(newBlock.width, currBlock.width) / 2.0f * (randomFactor == 0 ? -1 : 1);
				else if (newBlock.forceCenterCurrent) newPos.x = currBlock.transform.localPosition.x;
				nextPos.x = newPos.x;
				newBlock.transform.localPosition = newPos;
			}

			if (currBlock.forceCenter)
			{
				newBlock.transform.localPosition = new Vector3(currBlock.transform.localPosition.x, nextPos.y, nextPos.z);
				// newBlock.transform.localPosition = new Vector3(currBlock.transform.localPosition.x, nextPos.y, nextPos.z) - currBlock.padding;
				nextPos.x = newBlock.transform.localPosition.x;
			}
			else if (newBlock.pullBack)
			{
				Vector3 newPos = newBlock.transform.localPosition;
				int randomPull = Random.Range(0, 151);
				newPos.y += newBlock.padding.y + randomPull;
				nextPos.y += newBlock.padding.y + randomPull;
				newBlock.transform.localPosition = newPos;
			}

			newBlock.Initialize();

			// zFactor = 0.00001f;

			switch (newBlock.elevation)
			{
				case ElevationType.Ascend:
					nextPos.z += 1;
					lockX = true;
				break;

				case ElevationType.Descend:
					nextPos.z -= 1;
					lockX = true;
				break;
			}

			nextPos.z += zFactor;

//			if (!lockX) nextPos.x += Random.Range(6, newBlock.width) / 2.0f * (randomFactor == 0 ? -1 : 1);

			nextPos.y -= newBlock.length;

			//nextSpawnPoint = nextPos.y + (newBlock.length * 0.95f); //3.0f / 4.0f);
			nextSpawnPoint = nextPos.y + newBlock.length + (currBlock.length / 2.0f);

			if (spawnCoin)
			{
				Vector3 coinPos = newBlock.transform.localPosition;
				coinPos.z += 2.5f;
				coinPos.x += Random.Range(newBlock.width * -0.3f, newBlock.width * 0.3f);
				coinPos.y -= newBlock.length * Random.Range(0.3f, 0.7f);
				GameObject g = ZObjectMgr.Instance.Spawn3D(coin.name, coinPos);
				g.transform.localEulerAngles = Vector3.zero;
			}

			currBlock = newBlock;
		}
	}

	public void SpawnBlockPlain()
	{
		if (!disableSpawn)
		{
			bool firstBlock = false;
			bool lockX = false;

			Road newBlock;

			if (!currBlock)
			{
				numBlocks = 1;
				newBlock = ZObjectMgr.Instance.Spawn3D(m_prefabRoad[0].name, nextPos).GetComponent<Road>();
				currBlock = newBlock;
				firstBlock = true;

				newBlock.SetDimensions(250, 8);
			}
			else
			{
					newBlock = ZObjectMgr.Instance.Spawn3D(m_prefabRoad[0].name, nextPos).GetComponent<Road>();
					newBlock.SetDimensions(250, 8);
			}


			newBlock.transform.localRotation = Quaternion.identity;

			Vector3 newPos = newBlock.transform.localPosition;
			nextPos.x = newPos.x;
			newBlock.transform.localPosition = newPos;

			newBlock.Initialize();

			nextPos.y -= newBlock.length;

			nextPos.z += zFactor;

			nextSpawnPoint = nextPos.y + newBlock.length + (currBlock.length / 2.0f);
			//nextSpawnPoint = nextPos.y + (newBlock.length * 3.0f / 4.0f);

			// spawncoin
			/*
			if (true)
			{
				Vector3 coinPos = newBlock.transform.localPosition;
				coinPos.z += 2.5f;
				coinPos.x = 0;
				coinPos.y -= newBlock.length * Random.Range(0.3f, 0.7f);
				GameObject g = ZObjectMgr.Instance.Spawn3D(coin.name, coinPos);
				g.transform.localEulerAngles = Vector3.zero;
			}
			*/

			currBlock = newBlock;
		}
	}

	public override void Gamescene_Setuplevel(bool hardreset)
    {

		// zFactor = 0;
		numRingsSpawned = 0;
		currRing = 2;
		lastRot = 0;

		currentColor = -1;
		UpdateColor();

		for (int x = 0; x < ringMats.Count; x++)
		{
			ringMats[x].color = ringDefaultColor;
		}

		m_objectCharacter.gameObject.GetComponent<Rigidbody>().isKinematic = true;


		//       m_objectCharacter.transform.localPosition = new Vector3(-70.5f, -30.93f, 14.34f);
		m_objectCharacter.transform.localPosition = new Vector3(-70.5f, -30.93f, 40f);
		if (!GameScene.instance.wasUnlock) m_objectCharacter.gameObject.GetComponent<Rigidbody>().isKinematic = false;
		// m_objectCharacter.gameObject.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 200f);

		//SpawnInitialLevel ();
		for (int x = 0; x < numRingsShown; x++) 
		{
			GameObject newRing = ZObjectMgr.Instance.Spawn3D(m_prefabArcs.name, new Vector3(75.2f, 232f - (x * 150), -18.2f));
			newRing.transform.localEulerAngles = Vector3.zero;
			newRing.transform.GetChild(0).GetComponent<MeshRenderer>().material = ringMats[x];
			numRingsSpawned++;
		}

		nextPos = new Vector3(0, 0, 2);
		currBlock = null;
		nextSpawnPoint = 0;
		nextScore = -168;

		patterns.Shuffle();

		pattern = "BASIC";

		numBlocksToNextPattern = Random.Range(15, 20);

		numBlocksToNextSpecial = Random.Range(8, 12);
		// m_objectRoad.transform.localEulerAngles = Vector3.zero;
		SpawnBlockPlain();
    }

    public override void Gamescene_Unloadlevel()
    {

    }
}
