﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockXY : MonoBehaviour 
{
	public Vector3 pos;
	
	// Update is called once per frame
	void Update () 
	{
		Vector3 newPos = transform.position;
		newPos.x = -0.3888889f;
		newPos.y = 5.711895f;
		newPos.z = Camera.main.transform.position.z + 366.8f;
		transform.position = newPos;
	}
}
