﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


[System.Serializable]
public class BBTuningPatterns
{
	public List<Vector3> m_listPattern;
}

public enum BBStateEnum
{
	IDLE,
	CHARGE,
	SHOOT,
	RESULTS,
	SCORE
}

public class BBLevelMode : FDGamesceneInstance {

	public static BBLevelMode instance;
	public BBStateEnum m_eState;
	public GameObject m_objectCar;
	public GameObject m_objectSpawner;

	public GameObject m_prefabEnemyCar;
	public GameObject m_prefabEnemyCarPurple;
	public GameObject m_prefabEnemyCarGreen;
	public GameObject m_prefabProgression;
	public GameObject m_prefabGem;
	public GameObject m_prefabScore;

	public GameObject m_rootSpawnedObjects;

	public List<BBTuningPatterns> m_listTuningPatterns;
	public List<BBTuningPatterns> m_listTuningPatternsMedium;

	public List<int> m_listTuningDirection;
	public ParticleSystem m_particleBoost;
	public TrailRenderer m_trailBoost;
	public TrailRenderer m_trailBoost2;



	void Awake () 
	{
		instance = this;
		ZObjectMgr.Instance.AddNewObject (m_prefabEnemyCar.name, 50,this.m_rootSpawnedObjects);
		ZObjectMgr.Instance.AddNewObject (m_prefabEnemyCarPurple.name, 50,this.m_rootSpawnedObjects);
		ZObjectMgr.Instance.AddNewObject (m_prefabEnemyCarGreen.name, 50,this.m_rootSpawnedObjects);

		ZObjectMgr.Instance.AddNewObject (m_prefabGem.name, 50,this.m_rootSpawnedObjects);
		//ZObjectMgr.Instance.AddNewObject (m_prefabScore.name, 10,this.m_rootSpawnedObjects);
		m_eState = BBStateEnum.IDLE;
	}

	//public override void Gamescene_Update ()
	void Update()
	{
		//if( Input.mousePosition.x )
		if (BBLevelMode.instance.m_eState == BBStateEnum.RESULTS)
			return;
		if (Input.GetMouseButton (0)) {
			
			m_objectCar.transform.localPosition = new Vector3 (m_objectCar.transform.localPosition.x, 0, (Input.mousePosition.x - (Screen.width/2) )* 0.07f);
			if (m_objectCar.transform.localPosition.z >= 12.6f)
				m_objectCar.transform.localPosition = new Vector3 (m_objectCar.transform.localPosition.x, m_objectCar.transform.localPosition.y, 12.6f);
			if (m_objectCar.transform.localPosition.z <= -12.6f)
				m_objectCar.transform.localPosition = new Vector3 (m_objectCar.transform.localPosition.x, m_objectCar.transform.localPosition.y, -12.6f);


			Debug.Log ("Mouse Position : " + Input.mousePosition.x + ", Current Res : " + Screen.currentResolution.width);
		}
	}


	public override void Gamescene_Score ()
	{
		if (GameScene.instance.GetScore () > 15)
			SpawnLevel ();

		//m_particleBoost.Stop ();
	}

	public override void Gamescene_Die ()
	{
		ParticleSystem.EmissionModule emission = m_particleBoost.emission;
		emission.rateOverTime = 1;

		m_objectCar.GetComponent<Rigidbody> ().velocity = Vector3.zero;
		//m_objectCar.GetComponent<Rigidbody> ().isKinematic = true;
		m_objectCar.GetComponent<Rigidbody> ().useGravity = true;
		m_objectCar.GetComponent<Rigidbody> ().constraints = RigidbodyConstraints.None;
		m_objectCar.GetComponent<Rigidbody> ().AddForce (new Vector3 (Random.Range (-500, 500), Random.Range (500, 4000),0));
		m_objectCar.GetComponent<Rigidbody> ().AddTorque (new Vector3 (Random.Range (-1500, 1500), Random.Range (-1500, 1500), Random.Range (-1500, 1500)));
		m_eState = BBStateEnum.RESULTS;
	}

	float m_currentX;
	int m_currentIndex;
	float CAR_DISTANCE = 100f;
	//float CAR_DISTANCE = 200f;

	public void SpawnInitialLevel()
	{
		m_currentIndex = 0;
		m_currentX = -1200;

		for (int x = 0; x < 50; x++) {
			SpawnLevel ();
		}
	}

	bool isBlock = false;
	public void SpawnLevel()
	{
		GameObject obj;

		if (Random.Range (0, 100) > 98) {
			obj = ZObjectMgr.Instance.Spawn3D (m_prefabGem.name, new Vector3 (m_currentX, 0, 0));
		} else {
			int randomEnemyType = Random.Range (0, 100);
			if (randomEnemyType > 70) {
				obj = ZObjectMgr.Instance.Spawn3D (m_prefabEnemyCar.name, new Vector3 (m_currentX, 0, 0));
			} else if (randomEnemyType > 40) {
				obj = ZObjectMgr.Instance.Spawn3D (m_prefabEnemyCarPurple.name, new Vector3 (m_currentX, 0, 0));
			} else {
				obj = ZObjectMgr.Instance.Spawn3D (m_prefabEnemyCarGreen.name, new Vector3 (m_currentX, 0, 0));
			}
			obj.GetComponent<Rigidbody> ().isKinematic = true;
		}

		int randomPosition = Random.Range (0, 100);
		if (randomPosition > 67) {
			obj.transform.position -= new Vector3 (10f, 0, 0);
		} else if (randomPosition > 33) {
			obj.transform.position += new Vector3 (10f, 0, 0);
		} else {
		}
		obj.transform.localEulerAngles = new Vector3 (0f, 0f, 0);


		//if( m_currentIndex > 50 )
		//	obj.transform.localPosition += new Vector3 (0, 0, Random.Range (-5f, 5f));

		int randomDistance = Random.Range (0, 100);
		if (isBlock || randomDistance > 85) {
			m_currentX -= CAR_DISTANCE;
			m_currentX -= CAR_DISTANCE;
			isBlock = false;
		} else if (randomDistance > 70) {
			m_currentX -= CAR_DISTANCE;
		} else {
			isBlock = true;
		}
		//isBlock = true;
		//m_currentX -= Random.Range (180, 250);
		m_currentIndex++;
	}
	/*
	public void SpawnLevel()
	{
		GameObject obj;// = ZObjectMgr.Instance.Spawn3D (m_prefabEnemyCar.name, new Vector3 (m_currentX, 0, 0));

		int randomVisibleFalse = Random.Range (0, 100) % 4;
		for (int x = 0; x < 4; x++) {
			if (x != randomVisibleFalse) {
				obj = ZObjectMgr.Instance.Spawn3D (m_prefabEnemyCar.name, new Vector3 (m_currentX, 0, 0));

				obj.transform.position += new Vector3 (8f * x, 0, 0) - new Vector3 (13, 0, 0);

				/*int randomPosition = Random.Range (0, 100);
			if (randomPosition > 67) {
				obj.transform.position -= new Vector3 (10f, 0, 0);
			} else if (randomPosition > 33) {
				obj.transform.position += new Vector3 (10f, 0, 0);
			} else {
			}*/
			/*	obj.transform.localEulerAngles = new Vector3 (0f, 0f, 0);
			}
		}

		//if( m_currentIndex > 50 )
		//	obj.transform.localPosition += new Vector3 (0, 0, Random.Range (-5f, 5f));

		/*int randomDistance = Random.Range (0, 100);
		if (isBlock || randomDistance > 80) {
			m_currentX -= CAR_DISTANCE;
			m_currentX -= CAR_DISTANCE;
			isBlock = false;
		} else if (randomDistance > 60) {
			m_currentX -= CAR_DISTANCE;
		} else {
			isBlock = true;
		}isBlock = true;*/
		/*m_currentX -= Random.Range (180, 250);
		m_currentIndex++;
	}*/


	public override void Gamescene_Setuplevel(bool hardreset) 
	{
		SpawnInitialLevel ();

		m_objectCar.transform.localPosition = Vector3.zero;
		m_objectCar.transform.localEulerAngles = Vector3.zero;
		m_objectCar.GetComponent<Rigidbody> ().isKinematic = false;

		m_objectCar.GetComponent<Rigidbody> ().constraints = RigidbodyConstraints.FreezeRotation;
		m_trailBoost.Clear ();
		m_trailBoost2.Clear ();
		m_particleBoost.Clear ();
		ParticleSystem.EmissionModule emission = m_particleBoost.emission;
		emission.rateOverTime = 200;

		m_objectCar.GetComponent<BBCar> ().Reset ();
	//m_particleBoost.GetComponent(partic

		m_eState = BBStateEnum.IDLE;
	}

	public override void Gamescene_Unloadlevel() 
	{
	}

}
