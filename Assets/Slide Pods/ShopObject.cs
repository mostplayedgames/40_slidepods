﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopObject : MonoBehaviour 
{
	public string id;
	public int index;

	public Image shopObjImage;
	public Text shopObjName;
	public Image shopObjBackground;
	public Image shopObjBackgroundBorder;
	public Shadow shopObjShadow;

	public GameObject lockObject;
	public Text price;

	public Button changeCharButton;

	public Color selectedColor;
	public Color deselectedColor;
	public Color lockedColor;

	public Color selectedHighlightColor;
	public Color shadowColor;

	public void Start()
	{
		UpdatePrice();
	}

	public void UpdatePrice()
	{
//		price.text = GameScene.instance.m_coinsNeeded.ToString();
	}

	public void UpdatePrice(string newPrice)
	{
		if (price) price.text = newPrice;
	}

	public void Unlock()
	{
		if (GameScene.instance)
		{
			if (GameScene.instance.UnlockItem(id))
			{
				UpdateUnlocked();
			}
		}
		
	}

	public void UpdateUnlocked()
	{
		shopObjImage.gameObject.SetActive(true);
		lockObject.SetActive(false);
		changeCharButton.interactable = true;
		shopObjBackground.color = selectedColor;
		// shopObjBackground.color = new Color(1.0f, 0.875f, 0.0f);
	}

	public void UpdateSelected(bool locked, bool selected)
	{
		if (selected)
		{
			shopObjBackground.color = selectedColor;
			shopObjBackgroundBorder.color = selectedHighlightColor;
			shopObjShadow.effectColor = selectedHighlightColor;
		}
		else if (locked)
		{
			shopObjBackground.color = lockedColor;
			shopObjBackgroundBorder.color = lockedColor;
			shopObjShadow.effectColor = shadowColor;
		}
		else
		{
			shopObjBackground.color = deselectedColor;
			shopObjBackgroundBorder.color = deselectedColor;
			shopObjShadow.effectColor = shadowColor;
		}
	}

	public void ChangeSkin()
	{
		if (GameScene.instance) GameScene.instance.ChangeCharacter(id);
	}

	public void MakeEntry(string newId, int newIndex, Sprite newImg, bool locked, string name, bool current=false)
	{
		id = newId;
		index = newIndex;
		shopObjImage.sprite = newImg;

		lockObject.SetActive(locked);
//		shopObjName.text = name;
		shopObjImage.gameObject.SetActive(!locked);

		if (current)
		{
			shopObjBackground.color = selectedColor;
			shopObjBackgroundBorder.color = selectedHighlightColor;
			shopObjShadow.effectColor = selectedHighlightColor;

			changeCharButton.interactable = true;
		}
		else if (locked)
		{
			//			shopObjImage.color = new Color(0f, 0.5f, 1.0f);
			shopObjBackground.color = lockedColor;
			shopObjBackgroundBorder.color = lockedColor;
			shopObjShadow.effectColor = shadowColor;
			changeCharButton.interactable = false;
		}
		else
		{
			shopObjBackground.color = deselectedColor;// new Color(0f, 0.5f, 1.0f);
			shopObjBackgroundBorder.color = deselectedColor;
			shopObjShadow.effectColor = shadowColor;
			changeCharButton.interactable = true;
		}
	}

}
