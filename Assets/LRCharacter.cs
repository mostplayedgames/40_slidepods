﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LRCharacter : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter2D(Collision2D coll) {
		if (coll.gameObject.tag == "Die") {
			GameScene.instance.Die ();
		}
		if (coll.gameObject.tag == "Safe") {
			if (this.gameObject.GetComponent<Rigidbody2D> ().velocity.y < -31f) {
				GameScene.instance.Die ();
			} else {
				GameScene.instance.Score (1);
			}
		}
	}
}
