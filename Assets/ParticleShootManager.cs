﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum ParticleTypeEnum
{
	Classic_Particle,

	ShootOut_Ringless,
	ShootOut_RingShot,
	ShootOut_BoardShot,

	ShootOut_Special,
	ShootOut_Normal,
	Ground_Charge,

	CameraParticle
}

public class ParticleShootManager : ZSingleton<ParticleShootManager> 
{
	public GameObject m_objWhiteFlash;

	public AudioClip m_audioCamera;
	public AudioClip m_audioCrowd;

	public Text m_effectScoredPoints;
	public Text m_effectScoreTag;
	public ParticleSystem m_normalParticle;
	public ParticleSystem m_specialParticle;
	public ParticleSystem m_ringlessParticle;
	public ParticleSystem m_cameraParticles;

	public Color m_colorPlusTwo;

	public ParticleSystem m_classicParticle;

	public ParticleSystem m_chargedParticle;

	//public float m_crowdAnimSpeed;

	public List<GameObject> m_objSprite = new List<GameObject>();
	public List<Texture> m_listOfCrowdCheer = new List<Texture>();
	public List<Texture> m_listOfCrowdIdle = new List<Texture>();

	private Vector3 m_ringlessScale;

	void Awake()
	{
		//AnimateCrowd (1);
	}

	public void ShowChargedParticle(bool p_showParticle)
	{

	}

	public void DisableParticle(ParticleTypeEnum p_particleType)
	{
		switch (p_particleType) {
			case ParticleTypeEnum.ShootOut_Normal:
			{
				m_normalParticle.Stop ();
				m_normalParticle.gameObject.SetActive (false);
			}
			break;

			case ParticleTypeEnum.ShootOut_Special:
			{
				m_specialParticle.Stop ();
				m_specialParticle.gameObject.SetActive (false);
			}
			break;

			case ParticleTypeEnum.Ground_Charge:
			{
				m_chargedParticle.Stop ();
				m_chargedParticle.gameObject.SetActive (false);
			}
			break;

			case ParticleTypeEnum.Classic_Particle:
			{
				m_classicParticle.Stop ();
				m_classicParticle.gameObject.SetActive (false);
			}
			break;

			case ParticleTypeEnum.ShootOut_Ringless:
			{
				m_ringlessParticle.Stop ();
				m_ringlessParticle.gameObject.SetActive (false);
			}
			break;

			case ParticleTypeEnum.CameraParticle:
			{
				m_cameraParticles.Stop ();
				m_cameraParticles.gameObject.SetActive (false);
			}
			break;

			default:
			break;
		}
	}
	public void ShowIncrementingParticle( Vector3 p_position, int p_score, bool p_isBuzzer = false)
	{
		if (p_score < 3) {
			EnableNormalShootoutParticle (p_position, p_score, p_isBuzzer);
		} else {
			EnableRinglessParticle (p_position, p_score, p_isBuzzer);
		}
	}

	public void ShowParticle(ParticleTypeEnum p_particleType, Vector3 p_position)
	{
		switch (p_particleType) {

			case ParticleTypeEnum.ShootOut_Normal:
			{
				EnableNormalShootoutParticle (p_position);
			}
			break;

			case ParticleTypeEnum.ShootOut_Special:
			{
				EnableSpecialShootoutParticle (p_position);
			}
			break;

			case ParticleTypeEnum.Ground_Charge:
			{
				EnableChargeShotParticle (p_position);
			}
			break;

		case ParticleTypeEnum.Classic_Particle:
			{
				EnableClassicParticle(p_position);
			}
			break;

		case ParticleTypeEnum.ShootOut_Ringless:
			{
				EnableRinglessParticle (p_position);
				EnableCameraParticle ();
			}
			break;

		case ParticleTypeEnum.CameraParticle:
			{
			}
			break;

		default:
			break;
		}
	}

	private void EnableCameraParticle()
	{
		//m_cameraParticles.Play ();
		ZAudioMgr.Instance.PlaySFX(m_audioCamera, true);
		ZAudioMgr.Instance.PlaySFX (m_audioCrowd);
		m_cameraParticles.gameObject.SetActive (false);
		m_cameraParticles.gameObject.SetActive (true);
		m_cameraParticles.Stop ();
		m_cameraParticles.Play ();
		LeanTween.cancel (m_cameraParticles.gameObject);
		LeanTween.scale (m_cameraParticles.gameObject, m_cameraParticles.gameObject.transform.localScale, 2).setOnComplete (DisableCameraParticles);
	}

	private void DisableCameraParticles()
	{
		ZAudioMgr.Instance.StopSFX (m_audioCamera);
		DisableParticle (ParticleTypeEnum.CameraParticle);
	}

	private void EnableClassicParticle(Vector3 p_position)
	{
		EnableParticle (m_classicParticle, p_position);

	}

	private void EnableChargeShotParticle(Vector3 p_position)
	{
		EnableParticle (m_chargedParticle, p_position);
	}

	private void EnableRinglessParticle(Vector3 p_position, int p_score = 0, bool p_isBuzzer = false)
	{ 
		if (!GameScene.instance.IsLevelBased ()) {
			m_effectScoredPoints.transform.position = p_position - new Vector3 (20, 20, 0);
			m_effectScoreTag.transform.position = p_position - new Vector3 (20, 30, 0);
				
			m_effectScoreTag.gameObject.SetActive (true);

			m_ringlessScale = new Vector3 (0.5f, 0.5f, 0.5f);

			m_effectScoredPoints.gameObject.SetActive (true);
			if (p_score == 0) {
				m_effectScoredPoints.GetComponent<Text> ().text = "+3";
			} else {
				m_effectScoredPoints.GetComponent<Text> ().text = "+" + p_score;
			}
			m_effectScoredPoints.GetComponent<Text> ().color = Color.yellow;

			LeanTween.cancel (m_effectScoredPoints.gameObject);
			LeanTween.scale (m_effectScoredPoints.gameObject, m_ringlessScale,  0.8f).setEase (LeanTweenType.easeOutElastic);//36
			LeanTween.moveY (m_effectScoredPoints.gameObject, m_effectScoredPoints.transform.position.y + 56.0f, 0.8f).setEase (LeanTweenType.easeOutExpo).setOnComplete (ShortDelayScoreEffect);


			if (!p_isBuzzer) {
				m_effectScoreTag.text = GetTag (3);
			} else {
				m_effectScoreTag.text = GetTag (4);

			}
			m_effectScoreTag.color = Color.yellow;
			LeanTween.cancel (m_effectScoreTag.gameObject);
			LeanTween.scale (m_effectScoreTag.gameObject, m_ringlessScale,  0.8f).setEase (LeanTweenType.easeOutQuad);//26
			LeanTween.moveY (m_effectScoreTag.gameObject, m_effectScoreTag.transform.position.y + 10.0f, 0.8f).setEase (LeanTweenType.easeOutExpo);
			LeanTween.moveY (m_effectScoreTag.gameObject, m_effectScoreTag.transform.position.y + 46.0f, 0.8f).setEase (LeanTweenType.easeOutExpo);
		}

		EnableParticle (m_ringlessParticle, p_position);
		StartFreezeFrame ();
		ShowWhiteFlash ();
		//GameScene.instance.AddCoins (3);
		ZCameraMgr.instance.DoShake ();
	}

	private void EnableNormalShootoutParticle(Vector3 p_position, int p_score = 0, bool p_isBuzzer = false)
	{
		m_effectScoredPoints.transform.position = p_position - new Vector3 (20, 20, 0);

		m_effectScoreTag.transform.position = p_position - new Vector3 (20, 40, 0);
		m_effectScoreTag.gameObject.SetActive (true);

		m_ringlessScale = new Vector3 (0.4f, 0.4f, 0.4f);

		if (!GameScene.instance.IsLevelBased ())  
		{
			m_effectScoredPoints.gameObject.SetActive (true);
			m_effectScoredPoints.GetComponent<Text> ().color = Color.white;
			m_effectScoreTag.color = Color.white;

			if (p_score == 0) {
				m_effectScoredPoints.GetComponent<Text> ().text = "+1";
			} else {
				m_effectScoredPoints.GetComponent<Text> ().text = "+" + p_score;
				m_effectScoredPoints.GetComponent<Text> ().color = (p_score == 1) ? Color.white : m_colorPlusTwo;
				m_effectScoreTag.color = (p_score == 1) ? Color.white : m_colorPlusTwo;
			}
		}
			
		LeanTween.cancel (m_effectScoredPoints.gameObject);
		LeanTween.scale (m_effectScoredPoints.gameObject, m_ringlessScale, 0.8f).setEase (LeanTweenType.easeOutExpo);
		LeanTween.moveY (m_effectScoredPoints.gameObject, m_effectScoredPoints.transform.position.y + 46.0f, 0.8f).setEase (LeanTweenType.easeOutExpo).setOnComplete (ShortDelayScoreEffect);

		if (!p_isBuzzer) {
			m_effectScoreTag.text = GetTag (1);
		} else {
			m_effectScoreTag.text = GetTag (4);

		}
		//m_effectScoreTag.color = Color.white;
		LeanTween.cancel (m_effectScoreTag.gameObject);
		LeanTween.scale (m_effectScoreTag.gameObject, m_ringlessScale,  0.8f).setEase (LeanTweenType.easeOutExpo);
		LeanTween.moveY (m_effectScoreTag.gameObject, m_effectScoreTag.transform.position.y + 46.0f, 0.8f).setEase (LeanTweenType.easeOutExpo);


		EnableParticle (m_normalParticle, p_position);
		StartFreezeFrame ();
		//GameScene.instance.AddCoins (1);
		ZCameraMgr.instance.DoShake ();
	}

	private void EnableSpecialShootoutParticle(Vector3 p_position)
	{
		m_effectScoredPoints.transform.position = p_position - new Vector3 (20, 20, 0);

		if (GameScene.instance.IsLevelBased ()) {
			m_effectScoreTag.transform.position = p_position - new Vector3 (0, 20, 0);
		} else {
			m_effectScoreTag.transform.position = p_position - new Vector3 (20, 30, 0);
		}

		m_effectScoreTag.gameObject.SetActive (true);

		ShowWhiteFlash ();
		if (!GameScene.instance.IsLevelBased ()) 
		{
			m_effectScoredPoints.gameObject.SetActive (true);
			m_effectScoredPoints.GetComponent<Text> ().text = "+2";
			m_effectScoredPoints.GetComponent<Text> ().color = Color.yellow;
		}

		m_ringlessScale = new Vector3 (0.4f, 0.4f, 0.4f);
		LeanTween.cancel (m_effectScoredPoints.gameObject);
		LeanTween.scale (m_effectScoredPoints.gameObject, m_ringlessScale,  0.8f).setEase (LeanTweenType.easeOutExpo);//26 
		LeanTween.moveY (m_effectScoredPoints.gameObject, m_effectScoredPoints.transform.position.y + 46.0f,  0.8f).setEase (LeanTweenType.easeOutExpo).setOnComplete (ShortDelayScoreEffect);

		m_effectScoreTag.text = GetTag (2);
		m_effectScoreTag.color = Color.yellow;

		LeanTween.cancel (m_effectScoreTag.gameObject);
		LeanTween.scale (m_effectScoreTag.gameObject, m_ringlessScale, 0.8f).setEase (LeanTweenType.easeOutExpo);//26
		LeanTween.moveY (m_effectScoreTag.gameObject, m_effectScoreTag.transform.position.y + 46.0f, 0.8f).setEase (LeanTweenType.easeOutExpo);

		EnableParticle (m_specialParticle, p_position);
		StartFreezeFrame ();
		//GameScene.instance.AddCoins (1);
		ZCameraMgr.instance.DoShake ();
	}

	private void ShortDelayScoreEffect()
	{
		LeanTween.scale (m_effectScoredPoints.gameObject, m_ringlessScale, 0.5f).setEase(LeanTweenType.easeOutQuad).setOnComplete(ShrinkScoreEffect);
		LeanTween.scale (m_effectScoreTag.gameObject, m_ringlessScale, 0.5f).setEase(LeanTweenType.easeOutQuad);
	}

	private void ShrinkScoreEffect()
	{
		LeanTween.scale (m_effectScoredPoints.gameObject, new Vector3(0, 0, 0), 0.3f).setEase(LeanTweenType.easeOutElastic).setOnComplete(DisableScoreEffect);
		LeanTween.scale (m_effectScoreTag.gameObject, new Vector3(0, 0, 0), 0.3f).setEase(LeanTweenType.easeOutElastic);
	}

	private void DisableScoreEffect()
	{
		m_effectScoredPoints.gameObject.SetActive (false);
		m_effectScoreTag.gameObject.SetActive (false);
	}

	private void EnableParticle(ParticleSystem p_choseParticle, Vector3 p_position)
	{
		p_choseParticle.gameObject.transform.position = p_position;
		p_choseParticle.gameObject.SetActive (false);
		p_choseParticle.gameObject.SetActive (true);
		p_choseParticle.Stop ();
		p_choseParticle.Play ();
	}

	private void ShowWhiteFlash()
	{
		m_objWhiteFlash.gameObject.SetActive (true);
		LeanTween.cancel (this.gameObject);
		LeanTween.scale (m_objWhiteFlash, m_objWhiteFlash.transform.localScale, 0.2f).setOnComplete (DisableWhiteFlash);
	}

	private void DisableWhiteFlash()
	{
		m_objWhiteFlash.gameObject.SetActive (false);
	}

	private void StartFreezeFrame()
	{// 0.05
		LeanTween.scale (m_effectScoredPoints.gameObject, m_effectScoredPoints.transform.localScale, 0.05f).setOnComplete (StopFreezeFrame);
		//Time.timeScale = 0.4f;
	}

	private void StopFreezeFrame()
	{
		//m_effectScoredPoints.gameObject.SetActive (false);
		Time.timeScale = 1f;

	}

	private string GetTag(int p_shotState)
	{
		string retVal = "";
		int randVal = Random.Range (0, 5);

//		if (GameScene.instance.GetCurrentMode () == 1) {
//			if (FDShootOut.instance.DidReachedAverageScore ()) {
//				FDShootOut.instance.didReachedAverageScore = true;
//				return "AVERAGE SCORE!";
//			}
//		} else if (GameScene.instance.GetCurrentMode () == 2) {
//			if (FD3ptShootOut.instance.DidReachedAverageScore ()) {
//				FD3ptShootOut.instance.didReachedAverageScore = true;
//				return "AVERAGE SCORE!";
//			}
//		}

		switch (p_shotState) 
		{
			case 4: // BUZZER
			{
				retVal = "BUZZER";
			}
			break;

			case 3: // RINGLES QUOTES
			{
				if (randVal == 0) {
					retVal = "EPIC!";
				} else if (randVal == 1) {
					retVal = "ALL NET!";
				} else if (randVal == 2) {
					retVal = "INSANE";
				} else if (randVal == 3) {
					retVal = "LEGENDARY!";
				} else if (randVal == 4) {
					retVal = "MVP!";
				}
			}
			break;

			case 2:
			{
				if (randVal == 0) {
					retVal = "AMAZING!";
				} else if (randVal == 1) {
					retVal = "SWOOSH!";
				} else if (randVal == 2) {
					retVal = "ON FIRE!";
				} else if (randVal == 3) {
					retVal = "SUPERSTAR!";
				} else if (randVal == 4) {
					retVal = "GREAT!";
				}
			}
			break;

			case 1:
			{
				if (randVal == 0) {
					retVal = "NICE!";
				} else if (randVal == 1) {
					retVal = "GOOD SHOT!";
				} else if (randVal == 2) {
					retVal = "YAY!";
				} else if (randVal == 3) {
					retVal = "COOL!";
				} else if (randVal == 4) {
					retVal = "KOBY!";
				}
			}
			break;
		}

		return retVal;
	}

//	//1f
//	//0.5f
//	public void AnimateCrowd(float p_animSpeed)
//	{
//		m_crowdAnimSpeed = p_animSpeed;
//		AnimCrowdIdle1 ();
//	}
//
//	private void AnimCrowdIdle2()
//	{
//		m_objSprite [0].GetComponent<Renderer> ().material.mainTexture = m_listOfCrowdIdle[0];
//		m_objSprite [1].GetComponent<Renderer> ().material.mainTexture = m_listOfCrowdIdle[0];
//		LeanTween.cancel (m_objSprite [0].gameObject);
//		LeanTween.scale (m_objSprite [0].gameObject, m_objSprite[0].transform.localScale, m_crowdAnimSpeed).setOnComplete(AnimCrowdIdle1);
//	}
//
//	public void AnimCrowdIdle1()
//	{
//		m_objSprite [0].GetComponent<Renderer> ().material.mainTexture = m_listOfCrowdIdle[1];
//		m_objSprite [1].GetComponent<Renderer> ().material.mainTexture = m_listOfCrowdIdle[1];
//		LeanTween.cancel (m_objSprite [0].gameObject);
//		LeanTween.scale (m_objSprite [0].gameObject, m_objSprite[0].transform.localScale, m_crowdAnimSpeed).setOnComplete(AnimCrowdIdle2);
//	}
}
