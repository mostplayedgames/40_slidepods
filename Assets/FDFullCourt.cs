﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class FDFullCourt : FDGamesceneInstance 
{
	[Space]
	public static FDFullCourt instance;
	public FDShootOut_State m_eState;

	public GameObject m_spawnPoint;

	public Text m_textTries;
	public Text m_textScoreBanner;

	private SBBall m_curBall;
	private bool m_gameStarted;
	private float m_currentDeltaTime;
	private string m_prefabBallName;

	public void Awake()
	{
		instance = this;
	}

	public override void Gamescene_Setuplevel (bool hardreset = false) 
	{
		BoardManager.Instance.transform.position = new Vector3 (123, 54, 2);
		BoardManager.Instance.transform.eulerAngles = new Vector3 (0, 270, 0);
		BoardManager.Instance.transform.localScale = new Vector3 (120, 120, 120);
		BoardManager.Instance.GetComponent<BoardManager> ().EnableBoard ();

		m_textTries.gameObject.SetActive (true);
		UpdateAttempts (0);
		GameScene.instance.m_textTries2.text = "";
		UpdateCamera (CameraStats);
		GetNewBall ();

		m_gameStarted = false;
		m_eState = FDShootOut_State.IDLE;
	}

	public void GetNewBall()
	{
		m_prefabBallName = GameScene.instance.m_prefabBall.name;
		GameObject objBall = ZObjectMgr.Instance.Spawn2D (m_prefabBallName, m_spawnPoint.transform.position);
		m_curBall = objBall.GetComponent<SBBall>();
		m_curBall.ResetBall ();
		int currentBall = GameScene.instance.GetCurrentSelectedBird ();
		m_curBall.EnableBall (currentBall);
	}

	public override void Gamescene_ButtonUp ()
	{
		GameStart ();
	}

	public override void Gamescene_Score ()
	{
		GetNewBall ();
		BoardManager.Instance.AnimateNet ();
		GameScene.instance.PostScoreToLeaderboards ();
	}

	public override void Gamescene_Update ()
	{
		Controls ();
	}

	public override void Gamescene_Die ()
	{
		m_gameStarted = false;
	}

	public override void Gamescene_Unloadlevel ()
	{
		this.gameObject.SetActive (false);
		m_textTries.gameObject.SetActive (false);
	}

	private void Controls()
	{
		if (IsButtonPressed()) 
		{
			return;
		}

		if (m_curBall == null) {
			return;
		}


		if (Input.GetMouseButtonUp (0) && m_eState == FDShootOut_State.CHARGE) 
		{
			float timeDiff = Time.time - m_currentDeltaTime;
			Vector2 shootStrength = new Vector2 (100000 * 0.8f * timeDiff, 150000 * 0.8f * timeDiff);
			m_curBall.ShootBall (shootStrength);
			m_curBall = null;
			m_eState = FDShootOut_State.IDLE;
		} 
		else if (Input.GetMouseButtonDown (0) && m_eState == FDShootOut_State.IDLE) 
		{
			m_curBall.AnimateBall ();
			m_currentDeltaTime = Time.time;
			m_eState = FDShootOut_State.CHARGE;
		}
	}
	private void GameStart()
	{
		if (!m_gameStarted) 
		{
			m_gameStarted = true;
			GameScene.instance.m_currentScore = 0;
			GameScene.instance.m_textScoreShadow.text = "" + GameScene.instance.m_currentScore.ToString ("00");
			m_textScoreBanner.text = "SCORE";
		}
	}


}