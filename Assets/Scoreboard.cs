﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Scoreboard : MonoBehaviour {

	public Text m_textAverageScore;
	public Text m_textPerfectStreak;
	public Text m_textTotalShots;
	public Text m_textPlayerGrade;
	public Text m_textHighScore;

	public void Initialize(int p_shotsMade, int p_totalShots, int p_perfectStreak, int p_averageScore)
	{
		m_textTotalShots.text = "SHOTS MADE:\n" + p_shotsMade + "/" + p_totalShots;
//		m_textPerfectStreak.text = "PERFECT STREAK:\n" + p_perfectStreak;
		m_textAverageScore.text = "AVERAGE SCORE:\n" + p_averageScore;
		m_textPlayerGrade.text = "" + GameScene.instance.GetScore();
		m_textHighScore.text = "" + GameScene.instance.GetBestScore ();
	}

	public string GetPlayerGrade(int p_shotsMade, int p_shotsScored)
	{
		float percentage =  ((float)p_shotsMade / (float)p_shotsScored) * 100;
		Debug.Log ("");

		string retVal = "A";

		if (percentage > 80) {
			retVal = "S+";
		} else if (percentage > 75) {
			retVal = "S";
		} else if (percentage > 70) {
			retVal = "S-";
		} else if (percentage > 65) {
			retVal = "A+";
		} else if (percentage > 60) {
			retVal = "A";
		} else if (percentage > 55) {
			retVal = "A-";
		} else if (percentage > 50) {
			retVal = "B+";
		} else if (percentage > 45) {
			retVal = "B";
		} else if (percentage > 40) {
			retVal = "C+";
		} else if (percentage > 35) {
			retVal = "C";
		} else if (percentage > 30) {
			retVal = "C-";
		} else if (percentage > 25) {
			retVal = "D+";		
		} else if (percentage > 20) {
			retVal = "D";
		} else if (percentage > 15) {
			retVal = "D-";
		} else if (percentage > 10) {
			retVal = "F+";
		} else if (percentage > 5) {
			retVal = "F";
		} else {
			retVal = "F-";
		}
		return retVal;
	}
}
