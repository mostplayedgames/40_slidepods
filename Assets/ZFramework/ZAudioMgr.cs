﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ZAudioMgr : ZSingleton<ZAudioMgr> {

	List<AudioSource> m_listAudioSource;

	public bool mute = false;

	void Awake()
	{
		InstantiateAudioSources ();
	}

	void InstantiateAudioSources()
	{
		m_listAudioSource = new List<AudioSource> ();

		for(int x = 0; x < 25; x++) {
			m_listAudioSource.Add(this.gameObject.AddComponent<AudioSource>());
		}
	}

	AudioSource GetAvailableAudioSource()
	{
		foreach (AudioSource source in m_listAudioSource) {
			if (!source.isPlaying) {
				return source;
			}
		}
		return null;
	}

	public bool MuteUnmute()
	{
		mute = !mute;
		return mute;
	}


	public void Mute()
	{
		mute = false;
	}

	public void StopSFX(AudioClip clip)
	{
		for(int x = 0; x < 16; x++) {
			if( m_listAudioSource[x].clip == clip )
			{	m_listAudioSource[x].Stop();
			}
		}
	}

	public void PlaySFX(AudioClip clip, bool looping, float pitch)
	{
		AudioSource source = GetAvailableAudioSource ();

		source.pitch = pitch;
		source.loop = looping;
		source.clip = clip;
		if (mute) source.volume = 0; else source.volume = 1;
		source.Play ();
	}

	public void PlaySFX(AudioClip clip, bool looping)
	{
		AudioSource source = GetAvailableAudioSource ();

		source.pitch = 1;
		source.loop = looping;
		source.clip = clip;
		if (mute) source.volume = 0; else source.volume = 1;
		source.Play ();
	}

	public void PlaySFX(AudioClip clip)
	{
		AudioSource source = GetAvailableAudioSource ();

		if (source) {
			source.pitch = 1;
			source.loop = false;
			source.clip = clip;
			if (mute) source.volume = 0; else source.volume = 1;
			source.Play ();
		}
	}

	public void PlaySFX(AudioClip clip, float pitch)
	{
		AudioSource source = GetAvailableAudioSource ();

		source.pitch = pitch;
		source.loop = false;
		if (mute) source.volume = 0; else source.volume = 1;
		source.clip = clip;
		source.Play ();
	}

	public void PlaySFXVolume(AudioClip clip, float volume)
	{
		AudioSource source = GetAvailableAudioSource ();

		source.pitch = 1;
		source.loop = false;
		if (mute) source.volume = 0; 
		else source.volume = volume;
		source.clip = clip;
		source.Play ();
	}

	public void PlaySFXVolume(AudioClip clip, float volume, bool looping)
	{
		AudioSource source = GetAvailableAudioSource ();

		source.pitch = 1;
		source.loop = looping;
		if (mute) source.volume = 0;
		else source.volume = volume;
		source.clip = clip;
		source.Play ();
	}
}
