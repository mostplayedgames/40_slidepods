﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class FDAccuracyCourt : FDGamesceneInstance 
{
	[Space]
	public static FDAccuracyCourt instance;
	public FDShootOut_State m_eState;

	public GameObject m_spawnPoint;
	public GameObject m_rootBasket;
	public AudioClip m_audioBall;
	public AudioClip m_audioShoot;
	public Text m_textTries;
	public Text m_textScoreBanner;

	private GameObject m_prevBall;
	private GameObject m_curBall;
	private bool m_gameStarted;
	private float m_currentDeltaTime;
	private string m_prefabBallName;

	public void Awake()
	{
		instance = this;
	}


	public override void Gamescene_Setuplevel (bool hardreset = false) 
	{
		m_rootBasket.transform.position = new Vector3 (123, 54, 30);
		m_rootBasket.transform.eulerAngles = new Vector3 (0, 270, 0);
		LeanTween.cancel (m_rootBasket.gameObject);
		LeanTween.moveLocalZ (m_rootBasket.gameObject, m_rootBasket.transform.localPosition.z - 60, 3f).setLoopPingPong ();

		ParticleShootManager.Instance.DisableParticle (ParticleTypeEnum.Ground_Charge);

		m_prefabBallName = GameScene.instance.m_prefabBall.name;
		m_textTries.gameObject.SetActive (true);
		UpdateCamera (CameraStats);
		GetNewBall ();
		m_gameStarted = false;
		m_eState = FDShootOut_State.IDLE;
	}

	public void GetNewBall()
	{
		GameObject objBall = ZObjectMgr.Instance.Spawn2D (m_prefabBallName, m_spawnPoint.transform.position);
		m_curBall = objBall;
		m_curBall.GetComponent<SBBall>().ResetBall ();
		int currentBall = GameScene.instance.GetCurrentSelectedBird ();
		m_curBall.GetComponent<SBBall> ().EnableBall (currentBall);
		LeanTween.cancel (m_curBall);
	}

	public override void Gamescene_ButtonUp ()
	{
		GameStart ();
	}

	public override void Gamescene_Score ()
	{
		m_rootBasket.GetComponent<BoardManager> ().AnimateNet ();

//		int shotState = m_prevBall.GetComponent<SBBall>().m_shotState;
//		if (shotState == 3) {
//			ParticleShootManager.Instance.ShowParticle (ParticleTypeEnum.ShootOut_Ringless, m_rootBasket.transform.position);
//		} else if (shotState == 2) {
//			ParticleShootManager.Instance.ShowParticle (ParticleTypeEnum.ShootOut_Special, m_rootBasket.transform.position);
//		} else if (shotState == 1) {
//			ParticleShootManager.Instance.ShowParticle (ParticleTypeEnum.ShootOut_Normal, m_rootBasket.transform.position);
//		}
		bool isRingless = m_prevBall.GetComponent<SBBall> ().m_isRingless;
		if (isRingless) {
			ParticleShootManager.Instance.ShowParticle (ParticleTypeEnum.ShootOut_Ringless, m_rootBasket.transform.position);
		} else {
			ParticleShootManager.Instance.ShowParticle (ParticleTypeEnum.ShootOut_Normal, m_rootBasket.transform.position);
		}
		GetNewBall ();
		GameScene.instance.AddCoins (1);
	}

	public override void Gamescene_Update ()
	{
		Controls ();
	}

	public override void Gamescene_Die ()
	{
		m_gameStarted = false;
	}

	public override void Gamescene_Unloadlevel ()
	{
		this.gameObject.SetActive (false);
		m_textTries.gameObject.SetActive (false);
	}

	private void Controls()
	{
		if (m_curBall == null) 
		{
			return;
		}

		if (Input.GetMouseButtonUp (0) && m_eState == FDShootOut_State.CHARGE) 
		{
			float shootStrength = Time.time - m_currentDeltaTime;
			ShootBall (shootStrength);
			m_eState = FDShootOut_State.IDLE;
			ParticleShootManager.Instance.DisableParticle (ParticleTypeEnum.Ground_Charge);
		} 
		else if (Input.GetMouseButtonDown (0) && m_eState == FDShootOut_State.IDLE) 
		{
			AnimateBall ();
			m_currentDeltaTime = Time.time;
			m_eState = FDShootOut_State.CHARGE;
			ParticleShootManager.Instance.ShowParticle (ParticleTypeEnum.Ground_Charge, m_curBall.transform.position - new Vector3(3, 10, 0));
		}
	}

	private void AnimateBall()
	{
		m_curBall.GetComponent<SBBall>().ResetBall();
		LeanTween.move (m_curBall.gameObject, m_curBall.gameObject.transform.localPosition + new Vector3 (-5, -10, 0), 1.15f);
		LeanTween.scale (m_curBall.gameObject, new Vector3 (100, 20, 100), 1.15f);
		LeanTween.rotateZ(this.m_curBall.gameObject, -15f, 0.01f);
	}

	private void GameStart()
	{
		if (!m_gameStarted) 
		{
			m_gameStarted = true;
			//GameScene.instance.m_currentScore = 0;
			GameScene.instance.m_textScoreShadow.text = "" + GameScene.instance.m_currentScore.ToString ("00");
			m_textScoreBanner.text = "SCORE";
		}
	}

	private void ShootBall(float p_shotStrength)
	{
		LeanTween.cancel (m_curBall.gameObject);
		LeanTween.scale (m_curBall.gameObject, new Vector3 (100, 100, 100), 0.05f);
		m_curBall.GetComponent<Rigidbody2D>().isKinematic = false;
		m_curBall.GetComponent<Rigidbody2D>().AddForce (new Vector2 (30000 * p_shotStrength, 140000 * p_shotStrength));
		m_curBall.GetComponent<Rigidbody2D>().AddTorque (Random.Range(-105000,105000));
		m_prevBall = m_curBall;
		m_curBall = null;

		ZAudioMgr.Instance.PlaySFX(m_audioShoot);
	}


}