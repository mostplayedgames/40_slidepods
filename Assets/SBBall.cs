﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BallMaterial
{
	BOUNCY,
	SQUISHY,
	HEAVY
}

public enum Rarity
{
	COMMON,
	RARE,
	EPIC
}

[System.Serializable]
public class BallType
{
	public string ObjName;
	public string ObjDesc;
	public GameObject Obj;
	public BallMaterial Material;
	public Rarity BallRarity;
	public List<AudioClip> AudioList;
	public float minVolumeRange;
	public float maxVolumeRange;
	public Texture trailTexture;
}

public class SBBall : MonoBehaviour 
{
	public List<BallType> m_listOfBallTypes;

	public string m_tag;

	public bool m_isProps;
	public bool m_DidScore;
	public bool m_IsStillAlive;
	public bool m_isRingless;

	public GameObject m_fakeShadow;
	public AudioClip m_audioRingSfx;

	public AudioClip m_audioShoot;
	public AudioClip m_audioCharge;

	private List<AudioClip> m_listAudioBall;
	private int m_activeBall;

	public float m_slowMoTimer;

	void Update () 
	{
		if (!m_isProps) 
		{
			UpdateShadow ();
		}
	}

	private void UpdateShadow()
	{
		if (m_fakeShadow != null) 
		{
			Vector3 shadowPos = new Vector3 (this.transform.position.x, m_fakeShadow.transform.position.y, m_fakeShadow.transform.position.z);//-7
			m_fakeShadow.transform.position = shadowPos;
		}
	}

	public void EnableBall(int p_ball)
	{
		m_activeBall = p_ball;	

		foreach (BallType ball in m_listOfBallTypes) 
		{
			ball.Obj.SetActive (false);
		}

		m_listOfBallTypes[p_ball].Obj.SetActive(true);

		if (!m_isProps) 
		{
			m_fakeShadow = ZObjectMgr.Instance.Spawn3D ("BallShadow", Vector3.zero);
			//this.GetComponent<TrailRenderer> ().material.mainTexture = m_listOfBallTypes [p_ball].trailTexture;
		}
	}

	public void ShootBall(Vector2 p_force)
	{
		ZAudioMgr.Instance.StopSFX (m_audioCharge);
		ParticleShootManager.Instance.DisableParticle (ParticleTypeEnum.Ground_Charge);
		LeanTween.cancel (this.gameObject);
		LeanTween.scale (this.gameObject, new Vector3 (85, 85, 85), 0.05f);
		this.GetComponent<Rigidbody2D>().isKinematic = false;
		this.GetComponent<Rigidbody2D>().AddForce (p_force);
		this.GetComponent<Rigidbody2D>().AddTorque (Random.Range(-105000,105000));
		this.GetComponent<TrailRenderer> ().enabled = true;
		ZAudioMgr.Instance.PlaySFX(m_audioShoot);
	}

	public void AnimateBall(float p_curSensistivity = 1)
	{
		ResetBall ();
		ParticleShootManager.Instance.ShowParticle (ParticleTypeEnum.Ground_Charge, this.transform.position  - new Vector3(3, 10, 0));
		ZAudioMgr.Instance.PlaySFXVolume (m_audioCharge, 0.5f);
		float scaleTime = 1.15f / p_curSensistivity;
		if (scaleTime > 3) 
		{
			scaleTime = 3;
		}
		LeanTween.scale (this.gameObject, new Vector3 (100, 20, 100), scaleTime);
		LeanTween.rotateZ(this.gameObject, -15f, 0.01f);//-10
		LeanTween.move (this.gameObject, this.gameObject.transform.localPosition + new Vector3 (-5, -5, 0), scaleTime);
	}

	void OnDisable()
	{
		m_IsStillAlive = false;
	}

	public void ResetBall()
	{
		if (m_isProps) 
		{
			m_fakeShadow.gameObject.SetActive (false);
		}

		LeanTween.cancel (this.gameObject);

		ParticleShootManager.Instance.DisableParticle (ParticleTypeEnum.Ground_Charge);
		ZAudioMgr.Instance.StopSFX (m_audioCharge);

		m_activeBall = GameScene.instance.GetCurrentSelectedBird ();
		this.GetComponent<TrailRenderer> ().enabled = false;
		this.GetComponent<Rigidbody2D>().isKinematic = true;
		this.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
		this.GetComponent<Rigidbody2D>().angularVelocity = 0;

		this.transform.localEulerAngles = Vector3.zero;
		this.transform.localScale = new Vector3 (85, 85, 85);

		m_listAudioBall = GetActiveBall ().AudioList;

		m_tag = "";
		m_IsStillAlive = true;
		m_isRingless = true;
		m_DidScore = false;

	}

	private void BounceEffect()
	{
		Vector2 velocity = this.GetComponent<Rigidbody2D> ().velocity;
		float bounceFactor = 0;
		switch(GetActiveBall().Material)
		{
			case BallMaterial.BOUNCY:
			{
				bounceFactor = 1.1f;
			}
			break;
			case BallMaterial.SQUISHY:
			{
				bounceFactor = 1.5f;
			}
			break;

			case BallMaterial.HEAVY:
			{
				bounceFactor = 2f;
			}
			break;
		}
		this.GetComponent<Rigidbody2D> ().velocity = velocity / bounceFactor;


	}
		
	private BallType GetActiveBall()
	{
		return m_listOfBallTypes [m_activeBall];
	}
		
	void OnTriggerEnter2D(Collider2D coll)
	{
		if (coll.gameObject.tag == "Entrance" && m_tag == "") 
		{
			m_tag = "Entrance";
		}
		else if(coll.gameObject.tag == "Exit" && m_tag == "Entrance")
		{
			if (!m_IsStillAlive)
				return;
			
			m_tag = "EXIT";
			int score = (m_isRingless && !GameScene.instance.IsLevelBased () ? 3 : 1);
			int curMode = GameScene.instance.GetCurrentMode ();

			if (curMode == 0) {
				ScoreEffect1 ();
			} else if (curMode == 1) {
				ScoreEffect2 ();
			} else if (curMode == 2) {
				ScoreEffect3 (score);
			} else if (curMode == 3) {
				ScoreEffect4 (score);
			} else if (curMode == 4) {
				ScoreEffect5 (score);
			} else if (curMode == 5) {
				int testScore = (m_isRingless) ? 3 : 1;
				ScoreEffect6 (testScore);
			}

			m_DidScore = true;
		}
	}

	void OnCollisionEnter2D(Collision2D coll) 
	{
		if (coll.gameObject.tag == "Die")
		{
			this.GetComponent<TrailRenderer> ().enabled = false;

			BounceEffect ();
			Die ();

			m_IsStillAlive = false;
		} 
		else if (coll.gameObject.tag == "Board" || coll.gameObject.tag == "Ring") 
		{
			this.GetComponent<TrailRenderer> ().enabled = false;
			Vector3 curVel = this.GetComponent<Rigidbody2D> ().velocity/GetValue();
			this.GetComponent<Rigidbody2D> ().velocity = curVel;
			m_isRingless = false;
		}



		if (this.GetComponent<Rigidbody2D> ().velocity.y > 100) 
		{
			GameScene.instance.SpawnBallParticle (this.transform.localPosition);
		} 

		if (this.GetComponent<Rigidbody2D> ().velocity.y > 40) 
		{
			float min = m_listOfBallTypes [m_activeBall].minVolumeRange;
			float max = m_listOfBallTypes [m_activeBall].maxVolumeRange;

			ZAudioMgr.Instance.PlaySFXVolume (m_listAudioBall [Random.Range (0, 100) % m_listAudioBall.Count], Random.Range (min, max));
		} 
	}

	private void ScoreEffect1()
	{
		ParticleShootManager.Instance.ShowParticle (ParticleTypeEnum.ShootOut_Ringless, this.transform.position + new Vector3 (0, 20, 0));
		GameScene.instance.Score (1);
	}

	private void ScoreEffect2()
	{
		bool isBuzzer = false;
		if (FDShootOut.instance.m_timerVal <= 0 ) 
		{
			if (m_IsStillAlive) {
				isBuzzer = true;
			} else {
				return;
			}
		}

		int scoreIncrementer = FDShootOut.instance.m_scoreIncrementor;

		GameScene.instance.Score (FDShootOut.instance.m_scoreIncrementor, FDShootOut.instance.m_isGameStarted);
		ParticleShootManager.Instance.ShowIncrementingParticle (this.transform.position + new Vector3 (0, 20, 0), FDShootOut.instance.m_scoreIncrementor, isBuzzer);

		if (scoreIncrementer < 3) 
		{
			FDShootOut.instance.m_scoreIncrementor += 1;
		}
	}

	private void ScoreEffect3(int p_score)
	{
		GameScene.instance.Score (p_score);
		ParticleTypeEnum particleType = (p_score == 3) ? (ParticleTypeEnum.ShootOut_Ringless) : (ParticleTypeEnum.ShootOut_Normal);
		ParticleShootManager.Instance.ShowParticle (particleType, this.transform.position + new Vector3 (0, 20, 0));
	}

	private void ScoreEffect4(int p_score)
	{
		bool isBuzzer = false;

		if (FD3ptShootOut.instance.m_timerVal <= 0) 
		{
			if (m_IsStillAlive) {
				isBuzzer = true;
			} else {
				return;
			}
		}

		int scoreIncrementer = FD3ptShootOut.instance.m_scoreIncrementor;

		GameScene.instance.Score (FD3ptShootOut.instance.m_scoreIncrementor, FD3ptShootOut.instance.m_isGameStarted);
		ParticleShootManager.Instance.ShowIncrementingParticle (this.transform.position + new Vector3 (0, 20, 0), FD3ptShootOut.instance.m_scoreIncrementor, isBuzzer);

		if (scoreIncrementer < 3) 
		{
			FD3ptShootOut.instance.m_scoreIncrementor += 1;
		}
		//		GameScene.instance.Score (p_score, FD3ptShootOut.instance.m_isGameStarted);
		//		ParticleTypeEnum particleType = (p_score == 3) ? (ParticleTypeEnum.ShootOut_Ringless) : (ParticleTypeEnum.ShootOut_Normal);
		//		ParticleShootManager.Instance.ShowParticle (particleType, this.transform.position + new Vector3 (0, 20, 0));
		//
	}

	private void ScoreEffect5(int p_score)
	{
		GameScene.instance.Score (p_score);
		ParticleTypeEnum particleType = (p_score == 3) ? (ParticleTypeEnum.ShootOut_Ringless) : (ParticleTypeEnum.ShootOut_Normal);
		ParticleShootManager.Instance.ShowParticle (particleType, this.transform.position + new Vector3 (0, 20, 0));
	}

	private void ScoreEffect6(int p_score)
	{
		GameScene.instance.Score (p_score);
		ParticleTypeEnum particleType = (p_score == 3) ? (ParticleTypeEnum.ShootOut_Ringless) : (ParticleTypeEnum.ShootOut_Normal);
		ParticleShootManager.Instance.ShowParticle (particleType, this.transform.position + new Vector3 (0, 20, 0));
	}

	private float GetValue()
	{
		int attempts = GameScene.instance.GetTotalAttempts ();

		if (attempts < 60) {
			Debug.Log ("RAND");

			return Random.Range (0.95f, 1.3f);
		} else if (attempts < 90) {
			Debug.Log ("RAND2");

			return 1.1f;
		} else {
			Debug.Log ("RAND3");

			return 0.95f;
		}

//		if (GameScene.instance.GetBestScore () > 270) 
//		{
//			return 0.95f;
//		}
//		else if (GameScene.instance.GetBestScore () > 190) {
//			return 1.1f;
//		}
//		 else {
//			return 1.3f;
//		}
	}

	private void Die()
	{
		int curMode = GameScene.instance.GetCurrentMode ();

		switch (curMode) 
		{
			case 0:
			case 2:
			case 4:
			case 5:
			{
				if (!m_DidScore) 
				{
					GameScene.instance.Die ();
				}
			}
			break;

			case 1:
			{
				if (!m_DidScore && m_IsStillAlive) 
				{
					FDShootOut.instance.m_scoreIncrementor = 1;
				}
			}
			break;

			case 3:
			{
				if (!m_DidScore && m_IsStillAlive) {
					FD3ptShootOut.instance.m_scoreIncrementor = 1;
				}
			}
			break;
		}
	}
}
