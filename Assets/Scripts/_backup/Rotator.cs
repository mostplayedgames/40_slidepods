﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class Rotator : MonoBehaviour
{
	void Start()
	{
		RotateTo ();
	}

	void RotateTo()
	{
		LeanTween.rotateZ (this.gameObject, 30, 1f).setOnComplete(RotateFrom).setEase(LeanTweenType.easeInOutCubic);
	}

	void RotateFrom()
	{
		LeanTween.rotateZ (this.gameObject, -30, 1f).setOnComplete(RotateTo).setEase(LeanTweenType.easeInOutCubic);
	}

}
