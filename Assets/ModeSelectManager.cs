﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModeSelectManager : MonoBehaviour {


	//public List<ModesEntry> m_listOfModes = new List<ModesEntry> ();

	public List<ModesEntry> m_listOfModeEntries = new List<ModesEntry> ();
	public Color m_selcetedColor;
	public Color m_defaultColor;
	public Color m_newModeDefaultColor;
	private int m_idx;

	// Use this for initialization
	void OnEnable () 
	{
		m_idx = -1;
		int curMode = GameScene.instance.GetCurrentMode ();

		Debug.Log ("" + m_listOfModeEntries.Count);

		foreach (ModesEntry entry in m_listOfModeEntries) 
		{ 
			m_idx++;

			entry.Initialize (); 
			if (m_idx == curMode) 
			{
				entry.HighlightMode (m_selcetedColor);
				//LeanTween.scale (entry.gameObject, entry.transform.localScale + new Vector3 (0.2f, 0.2f, 0.2f), 0.2f).setLoopCount (2).setLoopPingPong ().setEase(LeanTweenType.easeInOutCubic);

				LeanTween.scale (entry.gameObject, entry.transform.localScale + new Vector3 (0.03f, 0.03f, 0.03f), 0.7f).setLoopPingPong ().setEase (LeanTweenType.easeInOutCubic);
			} 
			else 
			{
				if (m_idx >= 14) 
				{
					entry.HighlightMode (m_newModeDefaultColor);
					continue;
				}
				entry.HighlightMode (m_defaultColor);

			}
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}
}
