﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum ShootOutState
{
	IN_GAME,
	RESULTS
}

public class FDShootOut : FDGamesceneInstance 
{
	[Space]
	public static FDShootOut instance;
	public FDShootOut_State m_eState;

	// TODO Create a timer script
	public GameObject m_objTimerBar;
	public GameObject m_objBar;

	public GameObject m_spawnPoint;

	public AudioClip m_audioCountdown;
	public AudioClip m_audioBuzzer;
	public AudioClip m_audioBgm;

	public Scoreboard m_scoreBoard;
	public BallContainer m_ballContainer;

	public Text m_textTimesUpWarning;
	public Text m_textScoreBanner;

	public Color m_colorEnd;
	public Color m_colorStart;

	public int m_scoreIncrementor  =1;

	public float m_timerVal;

	public bool m_isGameStarted;

	private SBBall m_prevBall;
	[SerializeField]
	private SBBall m_curBall;

	private int m_totalShotsMade;
	private int m_shotCount; 
	private float m_currentDeltaTime;

	private string m_perfabBallName;

	private bool m_isTimerInDanger;

	public void Awake()
	{
		instance = this;
	}

	public override void Gamescene_Setuplevel (bool hardreset = false) 
	{
		m_perfabBallName = GameScene.instance.m_prefabBall.name;

		m_prevBall = m_curBall;
		UpdateCamera (CameraStats);
		GetNewBall ();
		SetupBallContainer ();

		BoardManager.Instance.transform.position = new Vector3 (123, 40, 1); 
		BoardManager.Instance.transform.eulerAngles = new Vector3 (0, 270, 0); 
		BoardManager.Instance.transform.localScale = new Vector3 (130, 130, 130);
	
		BoardManager.Instance.EnableBoard ();

		InitializeTimerBar ();

		m_textTimesUpWarning.gameObject.SetActive (false);
		m_textTimesUpWarning.transform.localScale = new Vector3 (0.7f, 0.7f, 0.7f);

		m_shotCount = 0;
		m_totalShotsMade = 0;
		m_timerVal = 30;
		m_scoreIncrementor = 1;
		m_isGameStarted = false;
		m_eState = FDShootOut_State.IDLE;
	}

	public void GetNewBall()
	{
		m_ballContainer.UpdateBall(m_shotCount % 4);

		GameObject objBall = null;
		for (int i = 0; i < 5; i++) 
		{
			objBall = ZObjectMgr.Instance.Spawn (m_perfabBallName);
			if (!objBall.GetComponent<SBBall> ().m_IsStillAlive) 
			{
				objBall.gameObject.SetActive (true);
				objBall.transform.position = m_spawnPoint.transform.position;
				objBall.GetComponent<Rigidbody2D> ().velocity = Vector2.zero;
				objBall.GetComponent<Rigidbody2D> ().angularVelocity = 0;
				objBall.GetComponent<SBBall> ().m_IsStillAlive = true;
				break;
			}
		}

		m_curBall = objBall.GetComponent<SBBall>();
		m_curBall.GetComponent<TrailRenderer> ().enabled = false;

		m_curBall.ResetBall ();
		int currentBall = GameScene.instance.GetCurrentSelectedBird ();
		m_curBall.EnableBall (currentBall);

		if (GameScene.instance.GetScore () > 0) 
		{
			m_shotCount++;
		}
	}


	public override void Gamescene_Score ()
	{		
		if (m_eState == FDShootOut_State.RESULTS) 
		{
			return;
		}
		BoardManager.Instance.AnimateNet ();
		m_totalShotsMade += 1;

		if (DidGameJustStarted()) 
		{
			ZAudioMgr.Instance.PlaySFXVolume (m_audioBgm, 0.5f, true);
			m_isGameStarted = true;
			LeanTween.cancel (m_objTimerBar.gameObject);
			m_objTimerBar.transform.localScale = new Vector3 (1, 1, 1);
			LeanTween.moveLocalY (m_objTimerBar.gameObject, m_objTimerBar.transform.localPosition.y - 70, 0.5f).setEase(LeanTweenType.easeInExpo);
			GameScene.instance.m_currentScore = GameScene.instance.m_scoreAdded;
			GameScene.instance.ShowScore (GameScene.instance.m_currentScore);
			m_textScoreBanner.text = "SCORE";
		}

		CourtManager.Instance.AnimateCrowd (0.5f);

		LeanTween.cancel (this.gameObject);
		LeanTween.scale (this.gameObject, this.transform.localScale, 2f).setOnComplete (ResetCrowdAnimation);
	}
		
	public override void Gamescene_Update ()
	{
		CountdownUpdate ();
		Controls ();
	}

	public override void Gamescene_Die ()
	{
	}

	public override void Gamescene_Unloadlevel ()
	{
		LeanTween.cancel (m_textTimesUpWarning.gameObject);
		LeanTween.cancel (this.gameObject);
		ZAudioMgr.Instance.StopSFX (m_audioCountdown);
		m_textTimesUpWarning.gameObject.SetActive (false);
		m_objTimerBar.gameObject.SetActive (false);
		m_scoreBoard.gameObject.SetActive (false);
		m_textTimesUpWarning.gameObject.SetActive (false);
		m_isTimerInDanger = false;
		ZAudioMgr.Instance.StopSFX (m_audioBgm);
		ZAudioMgr.Instance.StopSFX (m_audioBuzzer);
		this.gameObject.SetActive (false);
	}




	private void InitializeTimerBar()
	{
		m_objTimerBar.gameObject.SetActive (true);
		m_objTimerBar.transform.localScale = new Vector3 (1, 1, 1);
		RectTransform trans = m_objTimerBar.GetComponent<RectTransform> ();
		trans.anchoredPosition = new Vector3 (trans.anchoredPosition.x, 0, 0);
		m_objBar.GetComponent<Image> ().color = Color.green;

	}

	private void SetupBallContainer()
	{
		int currentBall = GameScene.instance.GetCurrentSelectedBird ();
		m_ballContainer.Initialize (currentBall);
		m_ballContainer.UpdateBall (m_shotCount % 4);
	}

//	private bool AreAllBallsDead()
//	{
//		for (int i = 0; i < 4; i++) 
//		{
//			GameObject objBall = ZObjectMgr.Instance.Spawn (m_perfabBallName);
//			if (objBall.GetComponent<SBBall> ().m_IsStillAlive) 
//			{
//				return false;
//			}
//		}
//		return true;
//	}

	private void CountdownUpdate()
	{
		if (!m_isGameStarted) 
		{
			return;
		}

		if (m_timerVal <= 0) 
		{
			ResetGame ();
		}
		else 
		{
			if(IsTimerInDanger())
			{
				ZAudioMgr.Instance.PlaySFX (m_audioCountdown, true);
				LeanTween.scale (m_objTimerBar.gameObject, m_objTimerBar.transform.localScale + new Vector3 (0.3f, 0.3f, 0.3f), 0.18f).setLoopPingPong ().setEase(LeanTweenType.easeOutExpo);
				m_objBar.GetComponent<Image> ().color = Color.red;
				m_isTimerInDanger = true;
			}
				 
			m_timerVal -= Time.deltaTime;
			m_objBar.gameObject.transform.localScale = new Vector3 (Mathf.Min(1, m_timerVal * 1.0f / 30), 6.93f, 1);
		}
	}

	public void ButtonCall_EnableRestartScreen()	
	{
		m_scoreBoard.gameObject.SetActive (false);
		m_textTimesUpWarning.gameObject.SetActive (false);
		GameScene.instance.ShowResultsScreen ();
		m_eState = FDShootOut_State.IDLE;
	}

	private void ShowTimesUpText()
	{
		m_textTimesUpWarning.color = m_colorEnd;
		m_textTimesUpWarning.text = "TIMES UP!";
		m_textTimesUpWarning.gameObject.SetActive (true);
		m_textTimesUpWarning.transform.localScale = new Vector3 (0.7f, 0.7f, 0.7f);
		LeanTween.scale (m_textTimesUpWarning.gameObject, m_textTimesUpWarning.transform.localScale + new Vector3 (0.5f, 0.5f, 0.5f), 0.5f).setLoopPingPong ().setLoopCount (2).setEase (LeanTweenType.easeOutExpo).setOnComplete(DelayDie);
	}

	private void ResetGame()
	{
		ZAudioMgr.Instance.StopSFX (m_audioCountdown);
		ZAudioMgr.Instance.StopSFX (m_audioBgm);
		ZAudioMgr.Instance.PlaySFX (m_audioBuzzer);		

		ShowTimesUpText ();
		HideTimerBar ();

		m_isGameStarted = false;
		m_isTimerInDanger = false;


		if (m_curBall != null) 
		{
			m_curBall.ResetBall ();
			m_curBall.gameObject.SetActive (false);
		}

		m_eState = FDShootOut_State.RESULTS;
		//GameScene.instance.PostScoreToLeaderboards ();
		GameScene.instance.m_eState = GAME_STATE.IDLE;
	}

	private void HideTimerBar()
	{
		LeanTween.cancel (m_objTimerBar.gameObject);
		LeanTween.moveLocalY (m_objTimerBar.gameObject, m_objTimerBar.transform.localPosition.y + 70, 0.5f).setEase (LeanTweenType.easeInExpo);
	}

	private void DelayDie()
	{
		LeanTween.cancel (this.gameObject);
		LeanTween.scale(this.gameObject, this.transform.localScale, 1).setOnComplete(ShowScoreboard);
	}


	private void ShowScoreboard()
	{
		m_scoreBoard.gameObject.SetActive (true);
		m_scoreBoard.Initialize (m_totalShotsMade, m_shotCount, 1, 1);
		GameScene.instance.PostScoreToLeaderboards ();
	}

	private void Controls()
	{
		if (IsButtonPressed()) 
		{
			return;
		}

		if (Input.GetMouseButtonUp (0) && m_eState == FDShootOut_State.CHARGE) 
		{
			float timeDiff = Time.time - m_currentDeltaTime;
			Vector2 shootStrength = new Vector2 (1.4f * 40000 * timeDiff, 1.4f * 120000 * timeDiff);
			m_curBall.ShootBall (shootStrength);
			m_curBall = null;

			m_eState = FDShootOut_State.IDLE;
		} 
		else if (Input.GetMouseButtonDown (0) && m_eState == FDShootOut_State.IDLE) 
		{
			if (CanGenerateNewBall()) 
			{
				GetNewBall ();
			}

			if(m_curBall != null)
			{
				m_curBall.AnimateBall ();
				m_currentDeltaTime = Time.time;
				m_eState = FDShootOut_State.CHARGE;
			}
		}
	}

	private bool CanGenerateNewBall()
	{
		if (m_curBall == null && m_timerVal > 0) {
			return true;
		} else {
			return false;
		}
	}

	private bool DidGameJustStarted()
	{
		if (GameScene.instance.GetScore () > 0 &&
			!m_isGameStarted &&
			m_timerVal > 0) 
		{
			return true;
		} else {
			return false;
		}
	}

	private bool IsTimerInDanger()
	{
		if (m_timerVal < 7 && !m_isTimerInDanger) {
			return true;
		} else {
			return false;
		}
	}

	private void ResetCrowdAnimation()
	{
		CourtManager.Instance.AnimateCrowd (1f);
	}
}
