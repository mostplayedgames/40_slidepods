﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ElevationType
{
	Ascend, Descend, Stay
}


public class Road : MonoBehaviour 
{
	public float length;
	public float width;
	public float thickness = 1;
	public Vector3 padding;

	public ElevationType elevation = ElevationType.Stay;

	public bool widthLocked = false;
	public bool lengthLocked = false;
	public bool usePrevWidth = false;

	public bool usePadding = false;
	public bool forceCenter = false;
	public bool forceCenterCurrent = false;
	public bool leanLeft = false;
	public bool leanRight = false;
	public bool pullBack = false;

	public bool unique = false;
	public bool encountered = false;

	public Renderer[] rends;
	public Material matScored;
	public Material matUnscored;

	public void SetDimensions(float l, float w)
	{
		if (!lengthLocked) length = l;
		if (!widthLocked) width = w;

		padding.x = 0;

	}

	public void Initialize()
	{
		transform.localScale = new Vector3((widthLocked ? transform.localScale.x : width), (lengthLocked ? transform.localScale.y : length), thickness);
		foreach (Renderer r in rends)
		{
			r.material = matUnscored;
		}
	}

	public void Score()
	{
		foreach (Renderer r in rends)
		{
	//		r.material = matScored;
		}
	}

	public void OnEnable()
	{
		Initialize();
	}
}
