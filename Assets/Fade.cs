﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fade : MonoBehaviour 
{
	public Color ogColor;
	private MeshRenderer mr;
	private Material mat;

	public Vector3 pos;

	private void Awake()
	{
		mr = GetComponent<MeshRenderer>();
		ogColor = mr.material.color;
		ogColor.a = 1;

		mat = mr.material;

		mat.color = ogColor;

		pos = Vector3.zero;
	}

	public void Update()
	{
		if (pos != transform.parent.localPosition)
		{
			FadeIn();
			pos = transform.parent.localPosition;
		}
	}

	private void FadeIn()
	{
//		ogColor = new Color(ogColor.r, ogColor.g, ogColor.b, 0);
//		mat.color = ogColor;

		for (float x = 0.2f; x <= 1; x += 0.2f)
		{
//			ogColor.a = x;
//			Debug.Log("Fading in... " + mr.material.name + x.ToString());
// 			LeanTween.delayedCall(0.2f + x, ColorRoutine);
		}
		
		// ogColor.a = 1;
		// LeanTween.color(gameObject, ogColor, 0.25f);
	}

	public void ColorRoutine()
	{
		mat.color = ogColor;
	}
}
