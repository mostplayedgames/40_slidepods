﻿using UnityEngine;
using System.Collections;

public class WaterBottleScoreChecker : MonoBehaviour 
{
	void OnTriggerEnter2D(Collider2D coll)
	{
		if (coll.gameObject.tag == "BALL" && coll.GetComponent<SBBall>().m_IsStillAlive) 
		{
			string gametag = coll.GetComponent<SBBall>().m_tag;
			if (gametag == "") 
			{
				coll.GetComponent<SBBall>().m_tag = this.gameObject.tag;
				gametag = this.gameObject.tag;
			}
			else if (gametag == "Entrance") 
			{
				coll.GetComponent<SBBall> ().m_tag = "Exit";
				int score = (coll.GetComponent<SBBall> ().m_isRingless && !GameScene.instance.IsLevelBased ()) ? 3 : 1;

				if (GameScene.instance.GetCurrentMode () == 0) {
					ScoreEffect1 ();
				} else if (GameScene.instance.GetCurrentMode () == 1) {
					ScoreEffect2 ();
				} else if (GameScene.instance.GetCurrentMode () == 2) {
					ScoreEffect3 (score);
				} else if (GameScene.instance.GetCurrentMode () == 3) {
					ScoreEffect4 (score);
				} else if (GameScene.instance.GetCurrentMode () == 4) {
					ScoreEffect5 (score);
				}
					
				coll.GetComponent<SBBall>().m_DidScore = true;
			}
		}
	}

	private void ScoreEffect1()
	{
		ParticleShootManager.Instance.ShowParticle (ParticleTypeEnum.ShootOut_Ringless, this.transform.position + new Vector3 (0, 20, 0));
		GameScene.instance.Score (1);
	}

	private void ScoreEffect2()
	{
		if (FDShootOut.instance.m_timerVal <= 0) 
		{
			return;
		}

		int scoreIncrementer = FDShootOut.instance.m_scoreIncrementor;

		GameScene.instance.Score (FDShootOut.instance.m_scoreIncrementor, FDShootOut.instance.m_isGameStarted);
		ParticleShootManager.Instance.ShowIncrementingParticle (this.transform.position + new Vector3 (0, 20, 0), FDShootOut.instance.m_scoreIncrementor);

		if (scoreIncrementer < 3) 
		{
			FDShootOut.instance.m_scoreIncrementor += 1;
		}
	}

	private void ScoreEffect3(int p_score)
	{
		GameScene.instance.Score (p_score);
		ParticleTypeEnum particleType = (p_score == 3) ? (ParticleTypeEnum.ShootOut_Ringless) : (ParticleTypeEnum.ShootOut_Normal);
		ParticleShootManager.Instance.ShowParticle (particleType, this.transform.position + new Vector3 (0, 20, 0));
	}

	private void ScoreEffect4(int p_score)
	{
		if (FD3ptShootOut.instance.m_timerVal <= 0) 
		{
			return;
		}

		int scoreIncrementer = FD3ptShootOut.instance.m_scoreIncrementor;

		GameScene.instance.Score (FD3ptShootOut.instance.m_scoreIncrementor, FD3ptShootOut.instance.m_isGameStarted);
		ParticleShootManager.Instance.ShowIncrementingParticle (this.transform.position + new Vector3 (0, 20, 0), FD3ptShootOut.instance.m_scoreIncrementor);

		if (scoreIncrementer < 3) 
		{
			FD3ptShootOut.instance.m_scoreIncrementor += 1;
		}
//		GameScene.instance.Score (p_score, FD3ptShootOut.instance.m_isGameStarted);
//		ParticleTypeEnum particleType = (p_score == 3) ? (ParticleTypeEnum.ShootOut_Ringless) : (ParticleTypeEnum.ShootOut_Normal);
//		ParticleShootManager.Instance.ShowParticle (particleType, this.transform.position + new Vector3 (0, 20, 0));
//
	}

	private void ScoreEffect5(int p_score)
	{
		GameScene.instance.Score (p_score);
		ParticleTypeEnum particleType = (p_score == 3) ? (ParticleTypeEnum.ShootOut_Ringless) : (ParticleTypeEnum.ShootOut_Normal);
		ParticleShootManager.Instance.ShowParticle (particleType, this.transform.position + new Vector3 (0, 20, 0));
	}

	private bool IsShotRingless(Rigidbody2D p_rb)
	{
		float impact = p_rb.velocity.y;
		return (impact < -190) ? (true) : (false);
	}
}
