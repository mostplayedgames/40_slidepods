﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum POLE_STATE{
	GAPFIXED,
	GAPMOVING,
	GAPCLOSING,
	GAPCHANGEPOS,
	GAPCHOMPING,
	GAPFIXEDANIM,
}
public class PoleObject : MonoBehaviour {

	public bool isHit = true;

	public GameObject m_objectScore1;
	public GameObject m_objectScore2;
	public GameObject m_objectScore3;
	public GameObject m_objectScore4;

	public GameObject m_topPipeObject; 
	public GameObject m_bottomPipeObject; 
	public GameObject m_topPipeObject2; 
	public GameObject m_bottomPipeObject2; 

	public Vector3 m_topPipePosition; 
	public Vector3 m_bottomPipePosition;

	public Vector2 m_difficultyChange;

	public TextMesh m_textMultiplier;

	public List<Material> m_listColorMaterials;

	public GameObject m_objectCoin;
	public GameObject m_objectSubCoin;

	public GameObject m_objectActivationChecker;

	public int m_debugTuning;

	int m_tuningDifficulty;
	int m_currentIndex;
	float m_gapDistanceTuning;

	POLE_STATE m_poleType;
	// Use this for initialization
	void Start () {
		//m_poleType = POLE_STATE.GAPFIXED;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Spawn(int tuningDifficulty, int currentIndex)
	{
		//Tuning_SetGapDistance (tuningDifficulty, currentIndex);
		m_tuningDifficulty = tuningDifficulty;
		m_currentIndex = currentIndex;

		int multiplierIndex = Mathf.FloorToInt((currentIndex+1) / 10f) + 1;
		if (multiplierIndex >= 5)
			multiplierIndex = 5;
		multiplierIndex--;
		
		//if (multiplierIndex >= 1)
		//	m_textMultiplier.text = "+" + multiplierIndex;
		//else
			m_textMultiplier.text = "";

//		switch (multiplierIndex) {
//		case 2:
//			m_textMultiplier.color = GameScene.instance.m_listTextMultiplierColor [1];
//			break;
//		case 3:
//			m_textMultiplier.color = GameScene.instance.m_listTextMultiplierColor [2];
//			break;
//		case 4:
//			m_textMultiplier.color = GameScene.instance.m_listTextMultiplierColor [3];
//			break;
//		case 5:
//			m_textMultiplier.color = GameScene.instance.m_listTextMultiplierColor [4];
//			break;
//		default:
//			m_textMultiplier.color = GameScene.instance.m_listTextMultiplierColor [0];
//			break;
//		}
		
		//Tuning_SetGapDistance ();



		if (currentIndex > 20) {
			//m_objectCoin.SetActive (true);
			SpawnCoin ();
		} else if (currentIndex > 15) {
			if (Random.Range (0, 100) > 20) {
				SpawnCoin ();
			} else {
				//m_objectCoin.SetActive (false);
			}
		} else if (currentIndex > 10) {
			if (Random.Range (0, 100) > 40) {
				//m_objectCoin.SetActive (true);
				SpawnCoin ();
			} else {
				//m_objectCoin.SetActive (false);
			}
		} else if (currentIndex > 5) {
			if (Random.Range (0, 100) > 60) {
				//m_objectCoin.SetActive (true);
				SpawnCoin ();
			} else {
				//m_objectCoin.SetActive (false);
			}
		} else if (currentIndex > 1) {
			if (Random.Range (0, 100) > 70) {
				//m_objectCoin.SetActive (true);
				SpawnCoin ();
			} else {
				//m_objectCoin.SetActive (false);

			}//m_objectCoin.SetActive (true);
		} else {
			//m_objectCoin.SetActive (false);
		}

		if (Random.Range (0,100) > 60) {
			m_objectCoin.SetActive (true);
			m_objectCoin.transform.localPosition = new Vector3 (0, -0.665f, 0);
		}



		switch( GameScene.instance.GetMode())
		{
		case 0: 
			m_objectActivationChecker.transform.localPosition = new Vector3 (2.17f, 0, 0);
			break;
		case 1: 
			m_objectActivationChecker.transform.localPosition = new Vector3 (1.17f, 0, 0);
			break;
		}

		// disable subs
		this.m_topPipeObject2.gameObject.SetActive (false);
		this.m_bottomPipeObject2.gameObject.SetActive (false);
	}

	void SpawnCoin()
	{
		return;
		GameObject coinObject;
		coinObject = ZObjectMgr.Instance.Spawn2D ("prefab_coin", Vector2.zero);

		LeanTween.cancel (m_objectCoin.gameObject);

		coinObject.transform.position = this.gameObject.transform.position + new Vector3 (Random.Range(-50f, -50f), -70f + Random.Range(-20f, 20f), 0);
		coinObject.transform.localScale = new Vector3 (125f, 125f, 125f);
		//m_objectSubCoin.GetComponent<ZRotator> ().enabled = true;
	}

	public void FlyCoin()
	{
		//m_objectSubCoin.GetComponent<ZRotator> ().enabled = false;
		//m_objectSubCoin.
		//m_objectSubCoin.transform.localEulerAngles = Vector3.zero;
	}

	public void SetPipeColor(int index)
	{
		this.m_topPipeObject.GetComponent<Renderer> ().material = m_listColorMaterials [index % m_listColorMaterials.Count];
		this.m_bottomPipeObject.GetComponent<Renderer> ().material = m_listColorMaterials [index % m_listColorMaterials.Count];
		this.m_topPipeObject2.GetComponent<Renderer> ().material = m_listColorMaterials [index % m_listColorMaterials.Count];
		this.m_bottomPipeObject2.GetComponent<Renderer> ().material = m_listColorMaterials [index % m_listColorMaterials.Count];


	}

	public void Tuning_SetGapDistance(float gapDistance)
	{
		LeanTween.cancel (m_bottomPipeObject);
		LeanTween.cancel (m_topPipeObject);
		m_gapDistanceTuning = Random.Range (m_difficultyChange.x, m_difficultyChange.y);

		int bestScore = GameScene.instance.GetBestScore ();
		int newBestScoreBasis = Mathf.FloorToInt(bestScore / 10f);

		if (m_currentIndex > 50 - bestScore) {
			m_gapDistanceTuning = Random.Range (-0.025f, -0.02f);
		} 
		else if (m_currentIndex > 40 - bestScore) {
			m_gapDistanceTuning = Random.Range (-0.02f, -0.01f);
		} 
		else if (m_currentIndex > 30 - bestScore) {
			m_gapDistanceTuning = Random.Range (0, -0.01f);
		} 
		else if (m_currentIndex > 20 - bestScore) {
			m_gapDistanceTuning = Random.Range (0, 0.02f);
		} else if (m_currentIndex > 10 - bestScore) {
			m_gapDistanceTuning = Random.Range (0f, 0.035f);
		} else if (m_currentIndex > 7 - bestScore) {
			m_gapDistanceTuning = Random.Range (0.035f, 0.04f);
		} else if (m_currentIndex > 5 - bestScore) {
			m_gapDistanceTuning = Random.Range (0.035f, 0.05f);
		} else {
			m_gapDistanceTuning = Random.Range (0.05f, 0.06f);
		}

		//m_gapDistanceTuning = 0.24f;
		//int randomDistance = Random.Range(0,100);

		//if (randomDistance > 30) {
		//if (m_currentIndex > 3 && (m_currentIndex+1) % 5 == 0) {
			//m_gapDistanceTuning = 0.22f;
			//m_gapDistanceTuning = 0.22f;
			//m_gapDistanceTuning = 0.19f;
		//	m_gapDistanceTuning = 0.18f;
		//	SetPipeColor (1);

		//} /*else if (randomDistance > 40) {
			//m_gapDistanceTuning = 0.22f;
		//	m_gapDistanceTuning = 0.25f;
		//	SetPipeColor (1);
		//} */else {
			//m_gapDistanceTuning = 0.19f;
			//m_gapDistanceTuning = 0.22f;
		//	SetPipeColor (0);

		float difficultyAdjust = 1f;
		if (m_currentIndex <= 5 && GameScene.instance.GetBestScore() < 5f ) { // TutorialSpawn
			m_gapDistanceTuning = 0.24f * difficultyAdjust;
		}
		else if (m_currentIndex <= 10 && GameScene.instance.GetBestScore() < 10f ) { // Tutorial
			m_gapDistanceTuning = 0.21f * difficultyAdjust;
		}
		else if (m_currentIndex >= 20) {
			//m_gapDistanceTuning = 0.2f;
			m_gapDistanceTuning = 0.19f * difficultyAdjust;
		} else if (m_currentIndex >= 10) {
			//m_gapDistanceTuning = 0.21f;
			m_gapDistanceTuning = 0.20f * difficultyAdjust;
		} else {
			//m_gapDistanceTuning = 0.22f
			m_gapDistanceTuning = 0.21f * difficultyAdjust;
		}

		m_gapDistanceTuning = gapDistance;

		//}
		//m_gapDistanceTuning = 0.15f; brutal
		//m_gapDistanceTuning = 0.19f; 

		//m_gapDistanceTuning = -0.045f;

		m_topPipeObject.transform.localPosition = m_topPipePosition + new Vector3 (0, m_gapDistanceTuning, 0);
		m_bottomPipeObject.transform.localPosition = m_bottomPipePosition - new Vector3 (0, m_gapDistanceTuning, 0);

		//float randomScaling = Random.Range (0.011f, 0.03f);
		//m_topPipeObject.transform.localScale = new Vector3 (randomScaling, 0.011f, 0.014f);
		//m_bottomPipeObject.transform.localScale = new Vector3 (randomScaling, 0.011f, 0.014f);

		//LeanTween.moveLocal (m_topPipeObject, m_topPipePosition + new Vector3(0,difficulty,0), 0.8f).setEase(LeanTweenType.easeInQuad);
		//LeanTween.moveLocal (m_bottomPipeObject, m_bottomPipePosition - new Vector3(0, difficulty,0), 0.8f).setEase(LeanTweenType.easeInQuad);
	}

	public void IntroAnim_Fixed()
	{
	}

	public void IntroAnim_ClawIn()
	{
	}

	public void IntroAnim_MoveIn()
	{
	}

	public void Behavior_GapFixed()
	{
		m_poleType = POLE_STATE.GAPFIXED;
	}

	public void Behavior_GapMoving ()
	{
		isActivate = false;
		m_poleType = POLE_STATE.GAPMOVING;
	}

	bool isActivate = true;
	public void Behavior_GapClosing ()
	{
		//ForceClose ();

		isActivate = false;
		m_poleType = POLE_STATE.GAPCLOSING;
	}

	public void Behavior_GapChangePos ()
	{
		m_poleType = POLE_STATE.GAPCHANGEPOS;
	}

	public void Behavior_Chomping ()
	{
		m_poleType = POLE_STATE.GAPCHOMPING;
	}

	public void Behavior_GapFixedAnim()
	{
		isActivate = false;
		m_poleType = POLE_STATE.GAPFIXEDANIM;
	}

	public void MoveFixedAnimPole()
	{
		GameObject spawnObject = this.gameObject;

		float exactYPosition = 0;
		exactYPosition = spawnObject.transform.position.y;
		if (spawnObject.transform.position.y > 87f) { // going up
			spawnObject.transform.position = new Vector3 (spawnObject.transform.position.x, 
				spawnObject.transform.position.y - 15f, 
				spawnObject.transform.position.z);

			LeanTween.moveY (spawnObject, exactYPosition, 0.7f);//.setLoopPingPong ();
		} else { // going down
			spawnObject.transform.position = new Vector3 (spawnObject.transform.position.x, 
				spawnObject.transform.position.y + 15f, 
				spawnObject.transform.position.z);

			LeanTween.moveY (spawnObject, exactYPosition, 0.7f);//.setLoopPingPong ();
		}

		switch( GameScene.instance.GetMode())
		{
		case 0: 
			m_objectActivationChecker.transform.localPosition = new Vector3 (1.17f, 0, 0);
			break;
		}


		//m_topPipeObject.transform.localPosition = m_topPipePosition + new Vector3 (0, m_gapDistanceTuning + 0.01f, 0);
		//m_bottomPipeObject.transform.localPosition = m_bottomPipePosition - new Vector3 (0, m_gapDistanceTuning + 0.01f, 0);

	}

	public void CancelAnims()
	{
		LeanTween.cancel (m_topPipeObject);
		LeanTween.cancel (m_bottomPipeObject);
		LeanTween.cancel (this.gameObject);
	}

	public void MovePole()
	{
		GameObject spawnObject = this.gameObject;

		float exactYPosition = 0;
		float randomSpeed = Random.Range (1.8f, 2.6f);
		randomSpeed = 2f;
		exactYPosition = spawnObject.transform.position.y;
		if (spawnObject.transform.position.y > 87f) { // going up
			spawnObject.transform.position = new Vector3 (spawnObject.transform.position.x, 
				spawnObject.transform.position.y - 50f, 
				spawnObject.transform.position.z);

			LeanTween.moveY (spawnObject, exactYPosition, randomSpeed).setLoopPingPong ();
		} else { // going down
			spawnObject.transform.position = new Vector3 (spawnObject.transform.position.x, 
				spawnObject.transform.position.y + 50f, 
				spawnObject.transform.position.z);

			LeanTween.moveY (spawnObject, exactYPosition, randomSpeed).setLoopPingPong ();
		}


		m_topPipeObject.transform.localPosition = m_topPipePosition + new Vector3 (0, m_gapDistanceTuning + 0.015f, 0);
		m_bottomPipeObject.transform.localPosition = m_bottomPipePosition - new Vector3 (0, m_gapDistanceTuning + 0.015f, 0);

	}

	public void ForceClose()
	{
		LeanTween.cancel (m_bottomPipeObject);
		LeanTween.cancel (m_topPipeObject);

		float randomStartPosition = 0.3f;// Random.Range (0.3f, 0.6f); // 0.31f
		//float randomStartPosition = 0.35f;// Random.Range (0.3f, 0.6f); // 0.31f



		//m_topPipeObject.transform.localPosition += new Vector3 (0, 0.47f, 0);
		//m_bottomPipeObject.transform.localPosition -= new Vector3 (0, 0.47f, 0);
		//float randomClose = Random.Range (3.2f, 3.2f);
		//float randomClose = 2.6f;
		float randomClose = 2.7f;

		switch( GameScene.instance.GetMode())
		{
		case 0: 
			randomStartPosition = 0.5f;
			randomClose = 3.5f;
			break;
		case 1: 
			randomStartPosition = 0.3f;
			randomClose = 2.7f;
			break;
		}

		m_topPipeObject.transform.localPosition = m_topPipePosition + new Vector3 (0, m_gapDistanceTuning + randomStartPosition, 0);
		m_bottomPipeObject.transform.localPosition = m_bottomPipePosition - new Vector3 (0, m_gapDistanceTuning + randomStartPosition, 0);

		//LeanTween.moveLocalY (m_topPipeObject, 0.44f, randomClose);//.setEase(LeanTweenType.easeInQuad);
		//LeanTween.moveLocalY (m_bottomPipeObject, -1.77f, randomClose);//.setEase(LeanTweenType.easeInQuad);
		LeanTween.moveLocalY (m_topPipeObject, 0.44f, randomClose);//.setEase(LeanTweenType.easeInBounce);
		LeanTween.moveLocalY (m_bottomPipeObject, -1.77f, randomClose);//.setEase(LeanTweenType.easeInBounce);
		//CloseIn ();
	}

	void CloseFromCurrentPos()
	{
		float randomClose = 1f;
		LeanTween.moveLocalY (m_topPipeObject, 0.44f, randomClose);//.setEase(LeanTweenType.easeInBounce);
		LeanTween.moveLocalY (m_bottomPipeObject, -1.77f, randomClose);
	}

	void CloseIn()
	{
		LeanTween.cancel (this.gameObject);
		LeanTween.cancel (m_bottomPipeObject);
		LeanTween.cancel (m_topPipeObject);
		float randomClose = Random.Range (2f, 2.8f);
		float randomStartPosition = Random.Range (0.3f, 0.5f); // 0.3f
		LeanTween.moveLocal (m_topPipeObject, m_topPipePosition + new Vector3 (0, -randomStartPosition, 0), randomClose);//.setEase(LeanTweenType.easeInQuad);
		LeanTween.moveLocal (m_bottomPipeObject, m_bottomPipePosition - new Vector3(0, -randomStartPosition,0), randomClose);//.setEase(LeanTweenType.easeInQuad);
	}

	public void Activate()
	{
		//return;
		//ForceClose ();
		//return;
		if (isActivate)
			return;
		else
			isActivate = true;
		
		Debug.Log ("Activate");
		switch (m_poleType) {
		case POLE_STATE.GAPMOVING:
				MovePole ();
			break;
		case POLE_STATE.GAPCLOSING:
				ForceClose ();
			break;
		case POLE_STATE.GAPFIXEDANIM:
				MoveFixedAnimPole ();
			break;
		}
	}

	public Color m_colorScore;

	public void Score()
	{
		isHit = true;
		//m_objectScore1.GetComponent<Renderer>().material.color = m_colorScore;
		//m_objectScore2.GetComponent<Renderer>().material.color = m_colorScore;

		LeanTween.color (m_objectScore1, m_colorScore, 0.2f);
		LeanTween.color (m_objectScore2, m_colorScore, 0.2f);
		LeanTween.color (m_objectScore3, m_colorScore, 0.2f);
		LeanTween.color (m_objectScore4, m_colorScore, 0.2f);

		GameScene.instance.m_particleScore1.transform.position = m_objectScore1.transform.position;
		GameScene.instance.m_particleScore2.transform.position = m_objectScore2.transform.position;
		GameScene.instance.m_particleScore1.gameObject.SetActive (true);
		GameScene.instance.m_particleScore2.gameObject.SetActive (true);
		//m_particleSystem.gameObject.GetComponent<ParticleSystem> ().spe

		/*if (GameScene.instance.GetScore() > 3 && (GameScene.instance.GetScore ()-1) % 5 == 0) {
			GameScene.instance.m_particleScore1.gameObject.GetComponent<ParticleSystem> ().startSpeed = 100f;
			GameScene.instance.m_particleScore2.gameObject.GetComponent<ParticleSystem> ().startSpeed = 100f;
			GameScene.instance.m_particleScore1.gameObject.GetComponent<ParticleSystem> ().Emit (Random.Range (55, 80));
			GameScene.instance.m_particleScore2.gameObject.GetComponent<ParticleSystem> ().Emit (Random.Range (55, 80));
			//Handheld.Vibrate ();
		} else {*/
			GameScene.instance.m_particleScore1.gameObject.GetComponent<ParticleSystem> ().startSpeed = 50f;
			GameScene.instance.m_particleScore2.gameObject.GetComponent<ParticleSystem> ().startSpeed = 50f;
			GameScene.instance.m_particleScore1.gameObject.GetComponent<ParticleSystem> ().Emit (Random.Range (15, 20));
			GameScene.instance.m_particleScore2.gameObject.GetComponent<ParticleSystem> ().Emit (Random.Range (15, 20));

		//}

		LeanTween.cancel (m_topPipeObject);
		LeanTween.cancel (m_bottomPipeObject);

		switch( GameScene.instance.GetMode())
		{
		case 0: 
			//CloseIn ();
			CloseFromCurrentPos();
			break;
		}
	}

	public void EmitSuccess()
	{
		GameScene.instance.m_particleScore1.gameObject.GetComponent<ParticleSystem> ().startSpeed = 100f;
		GameScene.instance.m_particleScore2.gameObject.GetComponent<ParticleSystem> ().startSpeed = 100f;
		GameScene.instance.m_particleScore1.gameObject.GetComponent<ParticleSystem> ().Emit (Random.Range (55, 80));
		GameScene.instance.m_particleScore2.gameObject.GetComponent<ParticleSystem> ().Emit (Random.Range (55, 80));
	}

	public void Reset(){
		isHit = false;
		m_objectScore1.GetComponent<Renderer>().material.color = Color.white;
		m_objectScore2.GetComponent<Renderer>().material.color = Color.white;
		m_objectScore3.GetComponent<Renderer>().material.color = Color.white;
		m_objectScore4.GetComponent<Renderer>().material.color = Color.white;


	}

	public void SpawnSubs(int count)
	{
		this.m_topPipeObject2.gameObject.SetActive (true);
		this.m_bottomPipeObject2.gameObject.SetActive (true);
	}


	public void Spawn_Old(int tuningDifficulty, int currentIndex)
	{

		/*
		//if (currentIndex <= 0)
		//	return;

		//m_topPipeObject.transform.localPosition += new Vector3 (0, 3, 0);
		//m_bottomPipeObject.transform.localPosition -= new Vector3 (0, 3, 0);


		float difficulty = Random.Range (m_difficultyChange.x, m_difficultyChange.y);

		if (currentIndex > 40) {
			difficulty = Random.Range (-0.05f, -0.02f);
		} 
		else if (currentIndex > 30) {
			difficulty = Random.Range (0, -0.02f);
		} 
		else if (currentIndex > 20) {
			difficulty = Random.Range (0, 0.02f);
		} else if (currentIndex > 10) {
			difficulty = Random.Range (0f, 0.035f);
		} else {
			difficulty = Random.Range (0f, 0.07f);
		}


		//LeanTween.moveLocal (m_topPipeObject, m_topPipePosition + new Vector3(0,difficulty,0), 0.8f).setEase(LeanTweenType.easeInQuad);
		//LeanTween.moveLocal (m_bottomPipeObject, m_bottomPipePosition - new Vector3(0, difficulty,0), 0.8f).setEase(LeanTweenType.easeInQuad);

		switch(GameScene.instance.m_birdType){
		case 4:
			LeanTween.cancel (m_bottomPipeObject);
			LeanTween.cancel (m_topPipeObject);
			if (Random.Range (0, 100) > 100) {
				LeanTween.moveLocal (m_topPipeObject, m_topPipePosition + new Vector3 (0, difficulty + 0.4f, 0), 0.8f).setOnComplete (CloseIn);//.setEase(LeanTweenType.easeInQuad);
				LeanTween.moveLocal (m_bottomPipeObject, m_bottomPipePosition - new Vector3 (0, difficulty + 0.4f, 0), 0.8f);//.setEase(LeanTweenType.easeInQuad);
			} else {
				m_topPipeObject.transform.localPosition = m_topPipePosition + new Vector3 (0, difficulty + 0.4f, 0);
				m_bottomPipeObject.transform.localPosition = m_bottomPipePosition - new Vector3 (0, difficulty + 0.4f, 0);

				LeanTween.moveLocalY (m_topPipeObject, m_topPipeObject.transform.localPosition.y + 0.5f, 0.8f).setLoopPingPong ();//.setEase(LeanTweenType.easeInQuad);
				LeanTween.moveLocalY (m_bottomPipeObject, m_bottomPipeObject.transform.localPosition.y + 0.5f, 0.8f).setLoopPingPong ();//.setEase(LeanTweenType.easeInQuad);
			}
			break;
		default:
			//LeanTween.moveLocal (m_topPipeObject, m_topPipePosition + new Vector3(0,difficulty,0), 0.8f).setEase(LeanTweenType.easeInQuad);
			//LeanTween.moveLocal (m_bottomPipeObject, m_bottomPipePosition - new Vector3(0, difficulty,0), 0.8f).setEase(LeanTweenType.easeInQuad);
			LeanTween.cancel (m_bottomPipeObject);
			LeanTween.cancel (m_topPipeObject);
			if (true){//Random.Range (0, 100) > 0) {
				LeanTween.moveLocal (m_topPipeObject, m_topPipePosition + new Vector3(0,difficulty,0), 0.8f).setEase(LeanTweenType.easeInQuad);
				LeanTween.moveLocal (m_bottomPipeObject, m_bottomPipePosition - new Vector3(0, difficulty,0), 0.8f).setEase(LeanTweenType.easeInQuad);
			} else {
				m_topPipeObject.transform.localPosition = m_topPipePosition + new Vector3(0,difficulty + 0.05f,0);
				m_bottomPipeObject.transform.localPosition = m_bottomPipePosition - new Vector3(0, difficulty- 0.05f,0) ;

				float timeRandom = Random.Range (0.5f, 1f);
				LeanTween.moveLocalY (m_topPipeObject, m_topPipeObject.transform.localPosition.y - 0.1f, timeRandom).setLoopPingPong ();//.setEase(LeanTweenType.easeInQuad);
				LeanTween.moveLocalY (m_bottomPipeObject, m_bottomPipeObject.transform.localPosition.y + 0.1f, timeRandom).setLoopPingPong ();//.setEase(LeanTweenType.easeInQuad);
			}
			break;
		}*/
	}

}
