﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum FGKickModeEnum
{
	IDLE,
	CHARGE,
	SHOOT,
	RESULTS,
	SCORE
}
public class FGKickMode : FDGamesceneInstance {

	public static FGKickMode instance;
	public FGKickModeEnum m_eState;

	public Text m_textTries;

	public FGCharacter m_scriptCharacter;
	public GameObject m_fgCharacter;

	public GameObject m_objectBall;
	public GameObject m_objectGround;
	public GameObject m_objectGoalBottom;
	public GameObject m_objectPlatform;

	public GameObject m_objectTitle;

	public List<Vector3> m_listTuning;
	public List<Vector3> m_listGroundTuning;
	public List<Vector3> m_listPlatformTuning;

	void Awake () 
	{
		instance = this;
	}

	//public override void Gamescene_Update ()
	void Update()
	{
		if (false && Input.GetMouseButtonDown (0)) {
			m_scriptCharacter.Kick ();
			m_objectTitle.SetActive (false);
		}
		if (false && Input.GetMouseButtonDown (1)) {
			m_scriptCharacter.KickBack ();
			//m_objectTitle.SetActive (false);
			m_objectTitle.SetActive (false);
		}

		if (Input.GetMouseButtonDown (0)) {
			m_objectTitle.SetActive (false);
		}
			

		if (Input.GetKeyDown (KeyCode.D)) {
			m_scriptCharacter.Kick ();
			m_objectTitle.SetActive (false);
		}
		if (Input.GetKeyDown (KeyCode.A)) {
			m_scriptCharacter.KickBack ();
			//m_objectTitle.SetActive (false);
			m_objectTitle.SetActive (false);
		}
		//if(Input.getkey
		//if (Input.GetMouseButtonUp (0)) {
		//	m_scriptCharacter.KickReset ();
			//m_objectTitle.SetActive (false);
		//}

		//ZCameraMgr.instance.gameObject.transform.localPosition = new Vector3 (m_fgCharacter.transform.localPosition.x + 6f, 
		//	ZCameraMgr.instance.gameObject.transform.localPosition.y, 
		//	ZCameraMgr.instance.gameObject.transform.localPosition.z);
		
	}
		

	public override void Gamescene_Score ()
	{
		Spawn ();
	}

	public GameObject m_objectGoal;

	void Spawn()
	{
		m_objectGoal.SetActive (true);
		float xPosition = m_listTuning [GameScene.instance.GetLevel ()].x;
		float yPosition = m_listTuning [GameScene.instance.GetLevel ()].y;
		float angle = m_listTuning [GameScene.instance.GetLevel ()].z;
		//m_objectGoal.transform.localPosition = new Vector3 (Random.Range (-0.36f, 15.49f), Random.Range (0.02f, 17.28f), 0);
		//m_objectGoal.transform.localPosition = new Vector3 (Random.Range (-0.36f, 15.49f), Random.Range (0.02f, 17.28f), 0);
		//m_objectGoal.transform.localEulerAngles = new Vector3 (0, 0, Random.Range (-90f - 45f, -90f + 45f));

		// RANGE
		// X  = -0.36f 15.49f
		// Y = 0.02f 17.28f
		// angle = -135f -35f
		m_objectGoal.transform.localPosition = new Vector3 (xPosition + 7f, yPosition + 5.1f, 0);
		m_objectGoal.transform.localEulerAngles = new Vector3 (0, 0, angle-90f);

		float groundAngle = m_listGroundTuning [GameScene.instance.GetLevel ()].x;
		float goalBehavior = m_listGroundTuning [GameScene.instance.GetLevel ()].y;
		float goalHalfGoal = m_listGroundTuning [GameScene.instance.GetLevel ()].z;

		m_objectGround.transform.localPosition = new Vector3 (0, -1.52f, 0);
		m_objectGround.transform.localEulerAngles = new Vector3 (0, 0, groundAngle);
		if( goalHalfGoal == 0 )
			m_objectGoalBottom.SetActive (true);
		else
			m_objectGoalBottom.SetActive (false);


		float platformXPosition = m_listPlatformTuning [GameScene.instance.GetLevel ()].x;
		float platformYPosition = m_listPlatformTuning [GameScene.instance.GetLevel ()].y;

		if (platformXPosition == 0 && platformXPosition == 0)
			m_objectPlatform.SetActive (false);
		else
			m_objectPlatform.SetActive (true);

		m_objectPlatform.transform.localPosition = new Vector3 (platformXPosition, platformYPosition, 0);
	}

	public override void Gamescene_Die ()
	{
	}


	public override void Gamescene_Setuplevel(bool hardreset) 
	{
		m_scriptCharacter.transform.localEulerAngles = Vector3.zero;
		m_scriptCharacter.transform.localPosition = Vector3.zero;
		m_objectBall.transform.localPosition = new Vector3 (6.7f, 0.67f, 0);
		m_objectBall.GetComponent<Rigidbody2D> ().velocity = Vector3.zero;
		m_objectBall.GetComponent<Rigidbody2D> ().angularVelocity = 0f;
		m_eState = FGKickModeEnum.IDLE;

		m_scriptCharacter.Reset ();
		Spawn ();

		//Debug.LogError ("Kick Mode Setup Level");
	}

	public override void Gamescene_Unloadlevel() 
	{
	}

}
