﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BBCar : MonoBehaviour {

	public GameObject m_boostEffect;
	public GameObject m_prefabScore;

	public GameObject m_objectBoost;

	// Use this for initialization
	void Start () {
		m_boostEffect.SetActive (false);
		//PlayerPrefs.DeleteAll ();
	}

	bool m_isBoost = false;
	float m_boostTime = 0;
	float m_boostCamera = 0;
	float m_boostInvincibleTime = 0;

	// Update is called once per frame
	void Update () {
		if (BBLevelMode.instance.m_eState == BBStateEnum.RESULTS)
			return;
		if (GameScene.instance.m_eState == GAME_STATE.RESULTS)
			return;
		if (GameScene.instance.m_eState == GAME_STATE.SHOP)
			return;
		if (GameScene.instance.m_eState == GAME_STATE.UNLOCK)
			return;
		//return;
		//this.transform.localPosition -= new Vector3 (500, 0, 0) * Time.deltaTime;


		Vector3 camPosition;
		if (m_isBoost) {
			camPosition = ZCameraMgr.instance.transform.localPosition;
			ZCameraMgr.instance.transform.localPosition = new Vector3 (camPosition.x, camPosition.y, this.transform.position.z - 90f);
			ZCameraMgr.instance.GetComponent<Camera> ().fieldOfView = 37.1f + m_boostCamera;
			m_boostCamera += Time.deltaTime * 50;
			if (m_boostCamera >= 20)
				m_boostCamera = 20;
			this.GetComponent<Rigidbody>().velocity = new Vector3(0,0,800f);
			m_boostTime -= Time.deltaTime;
	
			//if (m_boostTime <= 1) {
				
			//}
			if (m_boostTime <= 0) {
				m_isBoost = false;
				m_boostEffect.SetActive (false);
				m_objectBoost.SetActive (false);
			}
		} else {
			camPosition = ZCameraMgr.instance.transform.localPosition;
			ZCameraMgr.instance.transform.localPosition = new Vector3 (camPosition.x, camPosition.y, this.transform.position.z - 90f );
			ZCameraMgr.instance.GetComponent<Camera> ().fieldOfView = 37.1f + m_boostCamera;
			if (GameScene.instance.GetScore () > 200) {
				this.GetComponent<Rigidbody> ().velocity = new Vector3 (0, 0, 700f);
			}else if (GameScene.instance.GetScore () > 100) {
				this.GetComponent<Rigidbody> ().velocity = new Vector3 (0, 0, 600f);
			} else {
				this.GetComponent<Rigidbody> ().velocity = new Vector3 (0, 0, 500f);
			}
			m_boostCamera -= Time.deltaTime * 50;
			if (m_boostCamera <= 0)
				m_boostCamera = 0;

			m_boostInvincibleTime -= Time.deltaTime;
		}


	}

	void OnCollisionEnter(Collision collision)
	{
		if ((m_isBoost || m_boostInvincibleTime > 0) && collision.gameObject.GetComponent<Rigidbody> ()) {
			collision.gameObject.GetComponent<Rigidbody> ().isKinematic = false;
			collision.gameObject.GetComponent<Rigidbody> ().AddForce (new Vector3(Random.Range(-1000,1000), 5000, -0000));
			collision.gameObject.GetComponent<Rigidbody> ().AddTorque (new Vector3(2000, 2000, 2000));
			GameScene.instance.AddCoins (1);
		}
		else if (collision.gameObject.tag == "Enemy") {
			GameScene.instance.Die ();
		}
	}

	public void Reset()
	{
		m_boostCamera = 30;
	}

	private void OnTriggerEnter(Collider collision)
	{
		if (collision.gameObject.tag == "Safe") {
			GameScene.instance.Score (1);
			//ZObjectMgr.Instance.Spawn3D (m_prefabScore.name, collision.gameObject.transform.parent.transform.localPosition + new Vector3(0,8,10)).transform.localEulerAngles = new Vector3(0, 270f, 0);
		}
		if (collision.gameObject.tag == "Boost") {
			//GameScene.instance.Score (1);
			if (m_boostTime > 0) {
				m_boostTime = 6;
			} else {
				collision.gameObject.SetActive (false);
				m_isBoost = true;
				m_boostTime = 6;
				m_boostEffect.SetActive (true);
				m_boostCamera = 0;
				m_boostInvincibleTime = 1;
				m_objectBoost.SetActive (true);
			}
			//Debug.LogError ("Boost");
		}
	}
}
