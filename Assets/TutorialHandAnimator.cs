﻿using UnityEngine;
using System.Collections;

public class TutorialHandAnimator : MonoBehaviour {

	public Sprite m_spriteDown;
	public Sprite m_spriteUp;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	Vector3 currentPosition;

	public void AnimateUpperRight(Vector3 position)
	{
		LeanTween.cancel (this.gameObject);
		currentPosition = position;
		this.gameObject.transform.position = currentPosition;
		LeanTween.move (this.gameObject, this.gameObject.transform.position + new Vector3 (3, 8, 0), 0.6f).setOnComplete (AnimateRelease);
		this.GetComponent<SpriteRenderer> ().sprite = m_spriteDown;
	}

	public void AnimateRelease()
	{
		LeanTween.moveX (this.gameObject, this.gameObject.transform.position.x, 0.8f).setOnComplete(AnimateReset);
		this.GetComponent<SpriteRenderer> ().sprite = m_spriteUp;
	}

	public void AnimateReset()
	{
		this.transform.localPosition = currentPosition;
		AnimateUpperRight (currentPosition);
	}
}
