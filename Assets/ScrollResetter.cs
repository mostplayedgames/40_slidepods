﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollResetter : MonoBehaviour 
{
	ScrollRect sr;
	float numChars;
	float numRows = 3;
	int mod = 7;

	bool initialized = false;

	public void Start()
	{
		Initialize();
	}

	private void Initialize()
	{
		initialized = true;

		sr = GetComponent<ScrollRect>();

		numChars = GameScene.instance.playerCharacters.Count;

	}

	public void ResetScroll()
	{
		if (!initialized) Initialize();

		int currChar = GameScene.instance.currCharacter;
		int row = currChar / 3;

		if (sr) sr.verticalNormalizedPosition = 1.0f - (float) row / 6.0f;
	}

	private void OnEnable()
	{
		// sr.verticalNormalizedPosition = 0;
		ResetScroll();
	}
}
