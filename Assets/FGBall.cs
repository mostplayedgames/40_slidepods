﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FGBall : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter2D(Collision2D coll) 
	{
		if (coll.gameObject.tag == "Die") {
			this.GetComponent<Rigidbody2D> ().AddForce (new Vector3 (0, 12f, 0));
		}
	}
}
