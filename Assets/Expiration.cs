﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Expiration : MonoBehaviour 
{
	public float expiration = 0.5f;

	void Start () {
		expiration = 0.5f;
	}

	private void OnEnable()
	{
		LeanTween.delayedCall(expiration, Expire);
	}

	public void Expire()
	{
		gameObject.SetActive(false);
	}

}
